version: '3.7'

services:
 
  #CEPTD     
  suricata:
    image: registry.curex-project.eu:443/curex-local/kea_suricata:latest
    container_name: kea_suricata
    restart: unless-stopped
    network_mode: "host"
    cap_add:
      - NET_ADMIN
      - SYS_NICE
      - NET_RAW
    command: ONLINE
    volumes:
      - data_volume:/var/log/suricata
      - pcap_volume:/var/pcap
      
  elasticsearch:
    image: registry.curex-project.eu:443/curex-local/kea_elasticsearch:latest
    container_name: kea_elasticsearch
    restart: unless-stopped
    environment:
      - cluster.name=keacluster
      - node.name=keacluster-node-01
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms1024m -Xmx1024m"
      - xpack.ml.enabled=false
      - xpack.security.enabled=false
      - xpack.ilm.enabled=false
      - path.logs=/data/elk/log
      - path.data=/data/elk/data
      - http.host=0.0.0.0
      - http.cors.enabled=true
      - http.cors.allow-origin="*"
      - indices.query.bool.max_clause_count=2000
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - data_volume:/data
    networks:
      - "proxy-network"
    expose:
      - 9200
  
  logstash:
    image: registry.curex-project.eu:443/curex-local/kea_logstash:latest
    container_name: kea_logstash
    restart: unless-stopped
    volumes:
      - data_volume:/data
    networks:
      - "proxy-network"
  
  kibana:
    image: registry.curex-project.eu:443/curex-local/kea_kibana:latest
    container_name: kea_kibana
    restart: unless-stopped
    expose:
      - 5601
    networks:
      - "proxy-network"
    environment:
      - "VIRTUAL_HOST=kea-kibana.vlahavas.com"
      - "VIRTUAL_PORT=5601"
      - "LETSENCRYPT_HOST=kea-kibana.vlahavas.com"
      - "LETSENCRYPT_EMAIL=gvlahavas@csd.auth.gr"

  #Controller      
  webserver:
    image: registry.curex-project.eu:443/curex-local/kea_webserver:latest
    container_name: kea_webserver
    restart: unless-stopped
    depends_on:
      - api
    tty: true
    expose:
      - 80
    volumes:
      - api_volume:/var/www
    networks:
      - "proxy-network"
    environment:
      - "VIRTUAL_HOST=kea.curex-project.eu"
      - "VIRTUAL_PORT=80"
      - "LETSENCRYPT_HOST=kea.curex-project.eu"
      - "LETSENCRYPT_EMAIL=gvlahavas@csd.auth.gr"
  
  api:
    image: registry.curex-project.eu:443/curex-local/kea_api:latest
    container_name: kea_api
    restart: unless-stopped
    tty: true
    working_dir: /var/www
    volumes:
      - api_volume:/var/www
    environment:
      - "APP_NAME=${APP_NAME}"
      - "APP_ENV=${APP_ENV}"
      - "APP_DEBUG=${APP_DEBUG}"
      - "APP_URL=${APP_URL}"
      - "KIBANA_URL=${KIBANA_URL}"
      - "GRAFANA_URL=${GRAFANA_URL}"
      - "KIBANA_PORT=${KIBANA_PORT}"
      - "GRAFANA_PORT=${GRAFANA_PORT}"
      - "LOG_CHANNEL=${LOG_CHANNEL}"
      - "DB_CONNECTION=${DB_CONNECTION}"
      - "DB_HOST=${DB_HOST}"
      - "DB_PORT=${DB_PORT}"
      - "DB_DATABASE=${DB_DATABASE}"
      - "DB_USERNAME=${DB_USERNAME}"
      - "DB_PASSWORD=${DB_PASSWORD}"
      - "BROADCAST_DRIVER=${BROADCAST_DRIVER}"
      - "CACHE_DRIVER=${CACHE_DRIVER}"
      - "QUEUE_CONNECTION=${QUEUE_CONNECTION}"
      - "SESSION_DRIVER=${SESSION_DRIVER}"
      - "SESSION_LIFETIME=${SESSION_LIFETIME}"
      - "ELASTICSEARCH_HOST=${ELASTICSEARCH_HOST}"
      - "ELASTICSEARCH_PORT=${ELASTICSEARCH_PORT}"
      - "ELASTICSEARCH_SCHEME=${ELASTICSEARCH_SCHEME}"
      - "MQTT_HOST=${MQTT_HOST}"
      - "MQTT_PORT=${MQTT_PORT}"
      - "MQTT_DEBUG=${MQTT_DEBUG}"
      - "MQTT_QOS=${MQTT_QOS}"
      - "MQTT_RETAIN=${MQTT_RETAIN}"
      - "MLTD_HOST=${MLTD_HOST}"
      - "MLTD_PORT=${MLTD_PORT}"
      - "OD_HOST=${OD_HOST}"
      - "OD_PORT=${OD_PORT}"
      - "KEYCLOAK_REALM_PUBLIC_KEY=${KEYCLOAK_REALM_PUBLIC_KEY}"
      - "KEYCLOAK_LOAD_USER_FROM_DATABASE=${KEYCLOAK_LOAD_USER_FROM_DATABASE}"
      - "KEYCLOAK_USER_PROVIDER_CREDENTIAL=${KEYCLOAK_USER_PROVIDER_CREDENTIAL}"
      - "KEYCLOAK_TOKEN_PRINCIPAL_ATTRIBUTE=${KEYCLOAK_TOKEN_PRINCIPAL_ATTRIBUTE}"
      - "KEYCLOAK_APPEND_DECODED_TOKEN=${KEYCLOAK_APPEND_DECODED_TOKEN}"
      - "KEYCLOAK_ALLOWED_RESOURCES=${KEYCLOAK_ALLOWED_RESOURCES}"
      - "KEYCLOAK_REALM=${KEYCLOAK_REALM}"
      - "KEYCLOAK_URL=${KEYCLOAK_URL}"
      - "KEYCLOAK_CLIENT_ID=${KEYCLOAK_CLIENT_ID}"
      - "AUTH_ENABLED=${AUTH_ENABLED}"
      - "OD_GRAFANA_PARAMS=${OD_GRAFANA_PARAMS}"
      - "MLTD_GRAFANA_PARAMS=${MLTD_GRAFANA_PARAMS}"
      - "CEPTD_KIBANA_PARAMS=${CEPTD_KIBANA_PARAMS}"
      - "RSYSLOG_SERVER=${RSYSLOG_SERVER}"
      - "RSYSLOG_PORT=${RSYSLOG_PORT}"
    networks:
      - "proxy-network"

  #OD
  od:
    image: registry.curex-project.eu:443/curex-local/kea_od:latest
    container_name: kea_od
    depends_on:
      - timescaledb
    environment:
      - "RSYSLOG_SERVER=${RSYSLOG_SERVER}"
      - "RSYSLOG_PORT=${RSYSLOG_PORT}"
    restart: unless-stopped
    expose:
      - 9091
    networks:
      - "proxy-network"
  
  #MLTD
  mltd:
    image: registry.curex-project.eu:443/curex-local/kea_mltd:latest
    container_name: kea_mltd
    depends_on:
      - timescaledb
      - mosquitto
    environment:
      - "RSYSLOG_SERVER=${RSYSLOG_SERVER}"
      - "RSYSLOG_PORT=${RSYSLOG_PORT}"
    restart: unless-stopped
    expose:
      - 5000
    networks:
      - "proxy-network"
  
  #MQTT   
  mosquitto:
    image: registry.curex-project.eu:443/curex-local/kea_mqtt:latest
    container_name: kea_mqtt
    expose:
      - 1883
      - 9001
    volumes:
      - mqtt_data_volume:/mosquitto/data
      - mqtt_log_volume:/mosquitto/log
    networks:
      - "proxy-network"
  
  #Persistence
  timescaledb:
    image: registry.curex-project.eu:443/curex-local/kea_timescaledb:latest
    container_name: kea_timescaledb
    volumes:
      - timescaledb_volume:/var/lib/postgresql/data
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_DB=kea
    expose:
      - 5432
    networks:
      - "proxy-network"
  
  #Visualization
  grafana:
    image: registry.curex-project.eu:443/curex-local/kea_grafana:latest
    container_name: kea_grafana
    expose:
      - 3000
    environment:
      - GF_SECURITY_ALLOW_EMBEDDING=true
      - GF_SECURITY_COOKIE_SAMESITE=none
      - GF_AUTH_ANONYMOUS_ENABLED=true
      - GF_AUTH_ANONYMOUS_ORG_ROLE=Admin
      - "VIRTUAL_HOST=kea-grafana.vlahavas.com"
      - "VIRTUAL_PORT=3000"
      - "LETSENCRYPT_HOST=kea-grafana.vlahavas.com"
      - "LETSENCRYPT_EMAIL=gvlahavas@csd.auth.gr"
    networks:
      - "proxy-network"

networks:
  proxy-network:
    external:
      name: proxy-network


volumes:
  api_volume:
  data_volume:
  pcap_volume:
  mqtt_data_volume:
  mqtt_log_volume:
  timescaledb_volume:

