#!/bin/sh

sed "s|^APP_URL=.*|APP_URL=${APP_URL}|" .env.example | \
	sed "s|^GRAFANA_URL=.*|GRAFANA_URL=${GRAFANA_URL}|" | \
	sed "s|^GRAFANA_PORT=.*|GRAFANA_PORT=${GRAFANA_PORT}|" | \
	sed "s|^KIBANA_URL=.*|KIBANA_URL=${KIBANA_URL}" | \
	sed "s|^KIBANA_PORT=.*|KIBANA_PORT=${KIBANA_PORT}" > .env

php-fpm
