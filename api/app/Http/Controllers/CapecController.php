<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cviebrock\LaravelElasticsearch\Facade as Elasticsearch;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class CapecController extends Controller
{
    public function index(Request $request)
    {
        $perPage = $request->get('perPage', 10);
        $from = ($request->get('page', 1) - 1) * $perPage;

        $params = [
            'size' => $perPage,
            'from' => $from,
            'body' => [
                'query' => [
                    'match' => [
                        'type' => 'CAPEC'
                    ]
                ]
            ]
        ];

        $res = Elasticsearch::search($params);

        return new LengthAwarePaginator(
            $res['hits']['hits'],
            $res['hits']['total']['value'],
            $perPage,
            Paginator::resolveCurrentPage(),
            [ 'path' => Paginator::resolveCurrentPath() ]);
    }
}
