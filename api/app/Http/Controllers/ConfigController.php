<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function index(Request $request)
    {
        return [
            'base_url' => config('app.url'),
            'od_url' => config('kea.grafana.url') .
                (config('kea.grafana.port') ? ':' . config('kea.grafana.port') : '')
                . config('kea.grafana.od_params'),
            'mltd_url' => config('kea.grafana.url') .
                (config('kea.grafana.port') ? ':' . config('kea.grafana.port') : '')
                . config('kea.grafana.mltd_params'),
            'ceptd_url' => config('kea.kibana.url') .
                (config('kea.kibana.port') ? ':' . config('kea.kibana.port') : '')
                . config('kea.kibana.ceptd_params'),
        ];
    }

    public function keycloak(Request $request)
    {
        return [
            'realm' => config('keycloak.realm'),
            'url' => config('keycloak.url'),
            'clientId' => config('keycloak.client_id'),
        ];
    }
}
