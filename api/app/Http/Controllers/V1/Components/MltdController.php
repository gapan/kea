<?php

namespace App\Http\Controllers\V1\Components;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class MltdController extends Controller
{
    public function start(Request $request)
    {
        $data = [
            'model_id'  => 1,
            'mqtt_host' => config('mqtt.host'),
            'mqtt_port' => config('mqtt.port'),
            'mqtt_topic'    => 'xlsiem/event',
            'prediction_threshold'  => 0.003,
            'report_timedb_host' => env('DB_HOST'),
            'report_timedb_port' => env('DB_PORT'),
            'report_timedb_username' => env('DB_USERNAME'),
            'report_timedb_password' => env('DB_PASSWORD'),
            'report_timedb_database' => env('DB_DATABASE'),
            'report_timedb_table' => 'mltd',
            'report_timedb_ssl' => 'False',
            'report_asset_id' => 'server',
        ];

        $url = 'http://' . env('MLTD_HOST') . ':' . env('MLTD_PORT') . '/api/v1.0/mltd/prediction';

        $response = Http::withHeaders([
            'Application' => 'application/json'
        ])->post($url, $data);

        if (!isset($response->json()['process_id'])) {
            return response()->json(['error' => 'error'], 504);
        }

        $ret = [
            'component'  => 'mltd',
            'process_id' => $response->json()['process_id'],
            'created_at' => Carbon::now()
        ];

        DB::table('running_processes')->insert($ret);

        return $ret;
    }

    public function status(Request $request)
    {
        return DB::table('running_processes')
            ->where('component', '=', 'mltd')
            ->get()->toArray();
    }

    public function stop(Request $request, $pid)
    {
        $url = 'http://' . env('MLTD_HOST') . ':' . env('MLTD_PORT') . '/api/v1.0/mltd/prediction/stop/' . $pid;
        $response = Http::get($url);

        DB::table('running_processes')
            ->where('component', '=', 'mltd')
            ->where('process_id', '=', $pid)->delete();

        return $response;
    }

    public function train(Request $request, $train_id, $top)
    {
        $url = 'http://' . env('MLTD_HOST') . ':' . env('MLTD_PORT') . '/api/v1.0/mltd/threat-identification/' . $train_id . '/' . $top;
        $response = Http::get($url);
        return [
            'important_events' => explode(",", substr($response->json()['important_events'], 1, -1)),
            'timeframe' => $response->json()['timeframe']
        ];
    }

    public function count(Request $request)
    {
        if (!Schema::hasTable('mltd')) {
            return response()->json([
                'error' => 'Relation for MLTD component does not exist'
            ], 503);
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->get('start'));
        } else {
            $start = Carbon::now()->subHour();
        }

        if ($request->has('end'))
            $end = Carbon::parse($request->get('end'));
        else {
            $end = Carbon::now();
        }

        return [
            'count' =>
                DB::table('mltd')
                    ->whereBetween('created_on', [$start, $end])
                    ->count()
        ];
    }

    public function group(Request $request)
    {
        $request->validate([
            'unit' => 'required|string|in:second,minute,hour,day',
            'k' => 'required|integer',
            'limit' => 'nullable|integer'
        ]);

        if (!Schema::hasTable('mltd')) {
            return response()->json([
                'error' => 'Relation for MLTD component does not exist'
            ], 503);
        }

        $q = $request->get('k') . ' ' . $request->get('unit');
        $query = DB::table('mltd')
            ->select(DB::raw('count(*)'), DB::raw("time_bucket('" . $q . "', created_on) AS interval"))
            ->groupby('interval')
            ->orderBy('interval', 'desc');

        if ($request->has('limit')) {
            $query = $query->limit($request->get('limit'));
        }
        return $query->get();
    }

    public function last(Request $request)
    {
        $request->validate([
            'k' => 'required|integer',
        ]);

        if (!Schema::hasTable('mltd')) {
            return response()->json([
                'error' => 'Relation for MLTD component does not exist'
            ], 503);
        }

        Carbon::now()->format('Y-m-d');

        $start = Carbon::now()->subDays($request->get('k'))->format('Y-m-d');
        $end = Carbon::now()->format('Y-m-d');

        return DB::select("
        select series.day, coalesce(count, 0) as count from
            (
                SELECT  created_on::date as day, count(*)
                FROM    mltd
                WHERE  created_on::date >= '" . $start . "'::date
                GROUP BY day
            ) AS cnt
         right outer join
             ( SELECT ts::date as day FROM generate_series ( '" . $start . "'::date, '" . $end . "'::date, '1 day') AS ts )
        as series
            on series.day = cnt.day
            order by series.day;
        ");
    }
}
