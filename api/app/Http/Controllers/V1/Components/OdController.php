<?php

namespace App\Http\Controllers\V1\Components;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class OdController extends Controller
{
    public function start(Request $request)
    {
        $data = [
            'timeDb_database'=> env('DB_DATABASE'),
            'timeDb_host'=> env('DB_HOST'),
            'timeDb_password'=> env('DB_PASSWORD'),
            'timeDb_port'=> env('DB_PORT'),
            'timeDb_ssl'=> 'true',
            'timeDb_table'=> 'od',
            'timeDb_username'=> env('DB_USERNAME'),
            'k'=> '20',
            'measurement'=> 'packets-loss',
            "mqtt_host" => config('mqtt.host'),
            "mqtt_password"=> "",
            "mqtt_port"=> config('mqtt.port'),
            "mqtt_topic"=> "auth/od",
            "mqtt_usermane"=> "",
            "outlier_life"=> "0",
            "r"=> "0.1",
            "slide"=> "10",
            "w"=> "60"
        ];

        $url = 'http://' . env('OD_HOST') . ':' . env('OD_PORT') . '/api/v1/od';

        $response = Http::withHeaders([
            'Application' => 'application/json'
        ])->post($url, $data);

        if (!isset($response->json()['pid'])) {
            return response()->json(['error' => 'error'], 504);
        }

        $ret = [
            'component'  => 'od',
            'process_id' => $response->json()['pid'],
            'created_at' => Carbon::now()
        ];

        DB::table('running_processes')->insert($ret);

        return $ret;
    }

    public function status(Request $request)
    {
        return DB::table('running_processes')
            ->where('component', '=', 'od')
            ->get()->toArray();
    }

    public function stop(Request $request, $pid)
    {
        $url = 'http://' . env('OD_HOST') . ':' . env('OD_PORT') . '/api/v1/od/stop/' . $pid;
        $response = Http::get($url);

        DB::table('running_processes')
            ->where('component', '=', 'od')
            ->where('process_id', '=', $pid)->delete();

        return $response;
    }

    public function analyze(Request $request, $pid)
    {
        $url = 'http://' . env('OD_HOST') . ':' . env('OD_PORT') . '/api/v1/od/analyse/' . $pid;
        $response = Http::withHeaders([
            'Application' => 'application/json'
        ])->attach('file', $request->file('file')->get(), 'file')
        ->post($url);
        return $response;
    }

    public function count(Request $request)
    {
        if (!Schema::hasTable('od')) {
            return response()->json([
                'error' => 'Relation for OD component does not exist'
            ], 503);
        }

        if ($request->has('start')) {
            $start = Carbon::parse($request->get('start'));
        } else {
            $start = Carbon::now()->subHour();
        }

        if ($request->has('end'))
            $end = Carbon::parse($request->get('end'));
        else {
            $end = Carbon::now();
        }

        return [
            'count' =>
                DB::table('od')
                ->whereBetween('incident_date', [$start, $end])
                ->count()
        ];
    }

    public function group(Request $request)
    {
        $request->validate([
            'unit' => 'required|string|in:second,minute,hour,day',
            'k' => 'required|integer',
            'limit' => 'nullable|integer'
        ]);

        if (!Schema::hasTable('od')) {
            return response()->json([
                'error' => 'Relation for OD component does not exist'
            ], 503);
        }

        $q = $request->get('k') . ' ' . $request->get('unit');
        $query = DB::table('od')
            ->select(DB::raw('count(*)'), DB::raw("time_bucket('" . $q . "', incident_date) AS interval"))
            ->groupby('interval')
            ->orderBy('interval', 'desc');

        if ($request->has('limit')) {
            $query = $query->limit($request->get('limit'));
        }
        return $query->get();
    }

    public function last(Request $request)
    {
        $request->validate([
            'k' => 'required|integer',
        ]);

        if (!Schema::hasTable('od')) {
            return response()->json([
                'error' => 'Relation for OD component does not exist'
            ], 503);
        }

        Carbon::now()->format('Y-m-d');

        $start = Carbon::now()->subDays($request->get('k'))->format('Y-m-d');
        $end = Carbon::now()->format('Y-m-d');

        return DB::select("
        select series.day, coalesce(count, 0) as count from
            (
                SELECT  incident_date::date as day, count(*)
                FROM    od
                WHERE  incident_date::date >= '" . $start . "'::date
                GROUP BY day
            ) AS cnt
         right outer join
             ( SELECT ts::date as day FROM generate_series ( '" . $start . "'::date, '" . $end . "'::date, '1 day') AS ts )
        as series
            on series.day = cnt.day
            order by series.day;
        ");
    }
}
