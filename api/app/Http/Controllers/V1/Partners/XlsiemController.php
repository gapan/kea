<?php

namespace App\Http\Controllers\V1\Partners;

use App\Http\Controllers\Controller;
use App\Http\Requests\XlsiemRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Salman\Mqtt\Facades\Mqtt;

class XlsiemController extends Controller
{
    public function store(XlsiemRequest $request)
    {
        $dbEvents = [];
        $eventAlarms = [];
        foreach ($request->get('event_alarm') as $ev) {
            $dbEvents[] = [
                'asset_id' => $request->get('asset_id'),
                'event_alarm_id' => $ev['event_alarm_id'],
                'event_alarm_char' => $ev['event_alarm_char'],
                'name' => isset($ev['name']) ? $ev['name'] : null,
                'time' => Carbon::createFromTimestamp($request->get('timestamp')),
                'source_ip' => $ev['source_ip'],
                'source_port' => isset($ev['source_port']) ? $ev['source_port'] : null,
                'destination_ip' => $ev['destination_ip'],
                'destination_port' => isset($ev['destination_port']) ? $ev['destination_port'] : null,
                'priority' => isset($ev['priority']) ? $ev['priority'] : null,
                'confidence' => isset($ev['confidence']) ? $ev['confidence'] : null,
            ];
            $eventAlarms[] = [
                'event_alarm_id' => $ev['event_alarm_id'],
                'event_alarm_char' => $ev['event_alarm_char'],
                'name' => isset($ev['name']) ? $ev['name'] : null,
                'source_ip' => $ev['source_ip'],
                'source_port' => isset($ev['source_port']) ? $ev['source_port'] : null,
                'destination_ip' => $ev['destination_ip'],
                'destination_port' => isset($ev['destination_port']) ? $ev['destination_port'] : null,
                'priority' => isset($ev['priority']) ? $ev['priority'] : null,
                'confidence' => isset($ev['confidence']) ? $ev['confidence'] : null
            ];
        }
        $mqttEvents = array(
        	'asset_id' => $request->get('asset_id'),
        	'timestamp' => Carbon::createFromTimestamp($request->get('timestamp')),
        	'event_alarm' => $eventAlarms
        );
        

        // publish to MQTT Broker
        //Mqtt::ConnectAndPublish('xlsiem/event', json_encode($mqttEvents), $request->user()->id);
        Mqtt::ConnectAndPublish('xlsiem/event', json_encode($mqttEvents), 1);
        DB::table('xlsiem')->insert($dbEvents);

        return response()->json(['success' => 'success'], 200);
    }

    public function get()
    {
        return DB::table('xlsiem')->select([
            'asset_id',
            'event_alarm_id',
            'event_alarm_char',
            'name',
            'time',
            'source_ip',
            'source_port',
            'destination_ip',
            'destination_port',
            'priority',
            'confidence',
        ])->paginate(20);
    }
}
