<?php

namespace App\Http\Controllers\V1\Partners;


use App\Http\Controllers\Controller;
use App\Http\Requests\AdtRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Salman\Mqtt\Facades\Mqtt;

class AdtController extends Controller
{
    public function store(AdtRequest $request)
    {
        $dbEvents = [];
        $mqttEvents = [];
        foreach ($request->get('event_alarm') as $ev) {
            $dbEvents[] = [
                'asset_id' => $request->get('asset_id'),
                'event_alarm_id' => $ev['event_alarm_id'],
                'event_alarm_char' => $ev['event_alarm_char'],
                'name' => isset($ev['name']) ? $ev['name'] : null,
                'time' => Carbon::createFromTimestamp($request->get('timestamp')),
            ];
            $mqttEvents[] = [
                'asset_id' => $request->get('asset_id'),
                'event_alarm_id' => $ev['event_alarm_id'],
                'event_alarm_char' => $ev['event_alarm_char'],
                'name' => isset($ev['name']) ? $ev['name'] : null,
                'time' => $request->get('timestamp'),
            ];
        }

        // publish to MQTT Broker
        // Mqtt::ConnectAndPublish('adt/event', json_encode($mqttEvents), $request->user()->id);
        Mqtt::ConnectAndPublish('adt/event', json_encode($mqttEvents), 1);
        DB::table('adt')->insert($dbEvents);

        return response()->json(['success' => 'success'], 200);
    }

    public function get(Request $request)
    {
        return DB::table('adt')->select([
            'uuid',
            'time',
            'asset_id',
            'event_alarm_id',
            'event_alarm_char',
            'name'
        ])->paginate(20);
    }
}
