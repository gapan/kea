<?php

namespace App\Http\Controllers\V1\Partners;


use App\Http\Controllers\Controller;
//use App\Http\Requests\VdmRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VdmController extends Controller
{
    //public function store(VdmRequest $request)
    public function store()
    {
       // $records = [];

       // // merge two input arrays
       // $cve = $request->get('cve');
       // $cwe = $request->get('cwe');

       // $merged = array_merge($cve, $cwe);

       // foreach ($merged as $item) {
       //     $records[] = [
       //         'asset_id' => $request->get('asset_id'),
       //         'time' => Carbon::createFromTimestamp($request->get('timestamp')),
       //         'value' => $item
       //     ];
       // }
       // DB::table('vdm')->insert($records);
        Log::info('Received VDM report from IP '.\Request::ip().'. Storing into database');
        return response()->json(['success' => 'success'], 200);
    }

    public function get()
    {
        return DB::table('vdm')->select([
            'uuid',
            'time',
            'asset_id',
            'value',
        ])->paginate(20);
    }
}
