<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VdmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_id'      => 'required|string',
            'timestamp'     => 'required|numeric', // unix timestamp is numeric
            'cve'   => 'required|array',
            'cve.*'   => 'string',
            'cwe'   => 'required|array',
            'cwe.*'   => 'string',
        ];
    }
}
