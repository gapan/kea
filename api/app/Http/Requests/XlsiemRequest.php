<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class XlsiemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_id'      => 'required|string',
            'timestamp'     => 'required|numeric', // unix timestamp is numeric
            'event_alarm'   => 'required|array',
            'event_alarm.*.event_alarm_id' => 'string',
            'event_alarm.*.event_alarm_char' => 'required_with:event_alarm.*.event_alarm_id|string',
            'event_alarm.*.source_ip' => 'required_with:event_alarm.*.event_alarm_id|ipv4',
            'event_alarm.*.destination_ip' => 'required_with:event_alarm.*.event_alarm_id|ipv4',
        ];
    }
}
