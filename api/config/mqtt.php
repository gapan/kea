<?php


return [

    'host'     => env('MQTT_HOST','127.0.0.1'),
    'password' => env('MQTT_PASSWORD',''),
    'username' => env('MQTT_USERNAME',''),
    'certfile' => env('MQTT_CERT_FILE',''),
    'port'     => env('MQTT_PORT','1883'),
    'debug'    => env('MQTT_DEBUG',false), //Optional Parameter to enable debugging set it to True
    'qos'      => env('MQTT_QOS', 0), // set quality of service here
    'retain'   => env('MQTT_RETAIN', 0) // it should be 0 or 1 Whether the message should be retained.- Retain Flag
];
