<?php

return [

    'auth_enabled' => env('AUTH_ENABLED', false),

    'kibana' => [
        'url' => env('KIBANA_URL', null),
        'port' => env('KIBANA_PORT', null),
        'ceptd_params' => env('CEPTD_KIBANA_PARAMS', null),
    ],

    'grafana' => [
        'url' => env('GRAFANA_URL', null),
        'port' => env('GRAFANA_PORT', null),
        'od_params' => env('OD_GRAFANA_PARAMS', null),
        'mltd_params' => env('MLTD_GRAFANA_PARAMS', null),
    ],

    'remote_server' => env('RSYSLOG_SERVER', null),
    'remote_port' => env('RSYSLOG_PORT', null),
];
