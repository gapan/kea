<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$parameters = env('AUTH_ENABLED') ? ['middleware' => 'auth:api'] : [];

// keycloak endpoint should be auth free
Route::get('keycloak', 'ConfigController@keycloak');

Route::group($parameters, function () {
    Route::get('config', 'ConfigController@index');
    Route::get('capec', 'CapecController@index');
    Route::group(['prefix' => 'v1'], function () {

        Route::get('adt', 'V1\Partners\AdtController@get');
        Route::post('adt', 'V1\Partners\AdtController@store');
        Route::get('xlsiem', 'V1\Partners\XlsiemController@get');
        Route::post('xlsiem', 'V1\Partners\XlsiemController@store');
        Route::get('vdm', 'V1\Partners\VdmController@get');
        Route::post('vdm', 'V1\Partners\VdmController@store');

        Route::group(['prefix' => 'ceptd'], function () {
            Route::get('start', 'V1\Components\CeptdController@start');
            Route::get('status', 'V1\Components\CeptdController@status');
            Route::get('stop', 'V1\Components\CeptdController@stop');
        });
        Route::group(['prefix' => 'mltd'], function () {
            Route::get('start', 'V1\Components\MltdController@start');
            Route::get('status', 'V1\Components\MltdController@status');
            Route::get('stop/{pid}', 'V1\Components\MltdController@stop');
            Route::get('events/count','V1\Components\MltdController@count');
            Route::get('events/group','V1\Components\MltdController@group');
            Route::get('events/last','V1\Components\MltdController@last');
            //should be last since matches to anything */*
            Route::get('{train_id}/{top}', 'V1\Components\MltdController@train');
        });
        Route::group(['prefix' => 'od'], function () {
            Route::get('start', 'V1\Components\OdController@start');
            Route::get('status', 'V1\Components\OdController@status');
            Route::get('stop/{pid}', 'V1\Components\OdController@stop');
            Route::post('analyze/{pid}', 'V1\Components\OdController@analyze');
            Route::get('events/count','V1\Components\OdController@count');
            Route::get('events/group','V1\Components\OdController@group');
            Route::get('events/last','V1\Components\OdController@last');
        });
    });
});
