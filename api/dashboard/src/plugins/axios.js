import axios from 'axios'
import store from '~/store'
import Vue from 'vue'

// Request interceptor
axios.interceptors.request.use(request => {
  request.baseURL = process.env.VUE_APP_BASE_URL

  request.headers.Authorization = `Bearer ${Vue.prototype.$keycloak.token}`

  const locale = store.getters['lang/locale']
  if (locale) {
    request.headers.common['Accept-Language'] = locale
  }

  return request
}, error => {
  return Promise.reject(error)
})
