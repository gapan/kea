import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome'
import {
  faUser, faLock, faSignOutAlt, faEnvelope, faArrowDown, faArrowUp, faCogs, faBars, faStop, faSearch
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faUser, faLock, faSignOutAlt, faEnvelope, faArrowDown, faArrowUp, faCogs, faBars, faStop, faSearch
)

Vue.component('fa', FontAwesomeIcon)
Vue.component('fa-layers', FontAwesomeLayers)
