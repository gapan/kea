import * as types from '../mutation-types'

// state
export const state = {
  sidebarShow: true,
  sidebarMinimize: false
}

// getters
export const getters = {
  sidebarShow: state => state.sidebarShow,
  sidebarMinimize: state => state.sidebarMinimize
}

// mutations
export const mutations = {
  [types.TOGGLE] (state, variable) {
    state[variable] = !state[variable]
  }
}

// actions
export const actions = {
  toggle ({ commit }, variable) {
    commit(types.TOGGLE, variable)
  }
}
