function page (path) {
    return () => import(`~/pages/${path}`).then(m => m.default || m)
}

export default [
  {
    path: '/',
    name: 'panel',
    redirect: '/dashboard',
    component: page('panel/index.vue'),
    children: [
      {
        path: 'dashboard',
        name: 'panel.dashboard',
        component: page('panel/dashboard.vue')
      },
      {
        path: 'components/mltd',
        name: 'panel.components.mltd',
        component: page('panel/components/mltd.vue')
      },
      {
        path: 'components/od',
        name: 'panel.components.od',
        component: page('panel/components/od.vue')
      },
      {
        path: 'components/ceptd',
        name: 'panel.components.ceptd',
        component: page('panel/components/ceptd.vue')
      }
    ]
  }
  // { path: '/login', name: 'login', component: page('login.vue') }
]
