import 'core-js/stable'
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Toasted from 'vue-toasted'
import VueKeyCloak from '@dsb-norge/vue-keycloak-js'
import App from './App'
import router from './router'
import store from './store'
import '~/plugins'

import '~/assets/scss/custom.scss'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(Toasted, {
  position: 'bottom-center',
  duration: 5000
})

Vue.config.performance = true

Vue.use(VueKeyCloak, {
  config: process.env.VUE_APP_BASE_URL + '/keycloak',
  onReady: (keycloak) => {
    /* eslint-disable no-new */
    new Vue({
      router,
      store,
      ...App
    }).$mount('#app')
  }
})
