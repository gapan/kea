<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'       => 'Thanasis Naskos',
            'email'      => 'anaskos@csd.auth.gr',
            'password'   => Hash::make('password'),
            'is_admin'   => true
        ]);

        DB::table('users')->insert([
            'name'       => 'Chris Bellas',
            'email'      => 'chribell@csd.auth.gr',
            'password'   => Hash::make('password'),
            'is_admin'   => true
        ]);
    }
}
