<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adt', function (Blueprint $table) {
            $table->bigIncrements('uuid');
            $table->timestamp('time');
            $table->string('asset_id');
            $table->string('event_alarm_id');
            $table->string('event_alarm_char');
            $table->string('name')->nullable();
        });

        DB::raw("SELECT create_hypertable('adt', 'time', 'asset_id', 2);");
        DB::raw("CREATE INDEX ON adt (asset_id, time desc);");
        DB::raw("CREATE INDEX ON adt (time desc, asset_id);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adt');
    }
}
