<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateXlsiemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xlsiem', function (Blueprint $table) {
            $table->bigIncrements('uuid');
            $table->timestamp('time');
            $table->string('asset_id');
            $table->string('event_alarm_id');
            $table->string('event_alarm_char');
            $table->string('name')->nullable();
            $table->string('source_ip');
            $table->string('source_port')->nullable();
            $table->string('destination_ip');
            $table->string('destination_port')->nullable();
            $table->string('priority')->nullable();
            $table->string('confidence')->nullable();
        });

        DB::raw("SELECT create_hypertable('xlsiem', 'time', 'asset_id', 2);");
        DB::raw("CREATE INDEX ON xlsiem (asset_id, time desc);");
        DB::raw("CREATE INDEX ON xlsiem (time desc, asset_id);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xlsiem');
    }
}
