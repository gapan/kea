<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOdMltdRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE TABLE IF NOT EXISTS od(' .
                'uuid           serial PRIMARY KEY,' .
                'asset_id       VARCHAR (100) NOT NULL,' .
                'value          REAL NOT NULL,' .
                'incident_date  TIMESTAMP NOT NULL,' .
                'timeframe      VARCHAR (355) NOT NULL,' .
                'created_on     TIMESTAMP NOT NULL)');
        DB::statement('CREATE TABLE IF NOT EXISTS mltd(' .
                'uuid           serial PRIMARY KEY,' .
                'asset_id       VARCHAR (100) NOT NULL,' .
                'probability    REAL NOT NULL,' .
                'timeframe      VARCHAR (355) NOT NULL,' .
                'created_on     TIMESTAMP NOT NULL)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('od');
        Schema::dropIfExists('mltd');
    }
}
