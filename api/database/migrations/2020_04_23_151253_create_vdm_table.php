<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVdmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vdm', function (Blueprint $table) {
            $table->bigIncrements('uuid');
            $table->timestamp('time');
            $table->string('asset_id');
            $table->string('value');
        });

        DB::raw("SELECT create_hypertable('vdm', 'time', 'asset_id', 2);");
        DB::raw("CREATE INDEX ON vdm (asset_id, time desc);");
        DB::raw("CREATE INDEX ON vdm (time desc, asset_id);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vdm');
    }
}
