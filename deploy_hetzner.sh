#!/bin/sh

dump_logs() {
  CONTAINERS="suricata elasticsearch logstash kibana webserver api od mltd mqtt timescaledb grafana"
  for CONTAINER in $CONTAINERS; do
    echo "*** Logs for kea_$CONTAINER (start) ***"
    docker logs kea_$CONTAINER
    echo "*** Logs for kea_$CONTAINER (end) ***"
  done
}

# Login into the CUREX registry
docker login https://registry.curex-project.eu:443/artifactory/curex-local/

# pull all containers
docker-compose \
  -f docker-compose-hetzner.yml \
  pull

sleep 1

# Keep the timestamp of when starting up containers
TIME_START=`date +%s`

# Just in case something is up from previous runs
echo "Bringing down any previous containers that are still running..."
./delete_hetzner.sh

# Run containers
echo "Bringing up containers..."
docker-compose -f docker-compose-hetzner.yml up --detach

# loop until all containers are up
echo "Checking if all containers are up..."
while true; do
  # if any container other than composer has exited, bail out
  res=`docker ps -a | \
    grep "/curex-local/kea_" | \
    grep "Exited" | \
    wc -l`
  if [ $res -gt 0 ]; then
    echo "ERROR: Some containers have exited."
    docker ps -a | grep "^CONTAINER\|/curex-local/kea_"
    dump_logs
    docker-compose -f docker-compose-hetzner.yml down
    exit 1
  fi
  # count containers that are up
  res=`docker ps -a | \
    grep "/curex-local/kea_" | \
    grep "Up" | \
    wc -l`
  if [ $res -eq 11 ]; then
    echo "All good!"
    break
  fi
  # only wait for 10 mins until everything is up. It should be more than
  # enough.
  TIME_NOW=`date +%s`
  if [ $((TIME_NOW - TIME_START)) -ge 600 ]; then
    echo "ERROR: Timeout exceeded."
    dump_logs
    docker-compose -f docker-compose-hetzner.yml down
    exit 2
  fi
  sleep 1
done

# Wait a bit more
sleep 60

# create and populate the database
echo "Creating and populating the database..."
docker-compose -f docker-compose-hetzner.yml exec -T api php artisan migrate:fresh --seed --force

