/**
* Jenkinsfile to deploy multiple Docker containers based on docker-compose into a DEV server and run any test.
* This pipeline will run the Docker containers, execute the tests and then stop and remove the containers from the DEV  
* server automatically.
**/


pipeline {

  agent any

    environment {
      // Keep as is the following two lines
      PRIVATE_REGISTRY = "https://registry.curex-project.eu:443/curex-local/"
      ARTIFACTORY_URL = "registry.curex-project.eu:443/curex-local/"

      HOST_IP = "116.203.166.220"
      DEPLOYMENT_HOST = "${HOST_IP}:2376"
      DEPLOYMENT_HOST_CREDENTIALS = "vm2-creds"
    }

  stages {
           
    stage('Stop and remove the Docker containers in DEV server') {
      steps {
        script {
          docker.withServer("$DEPLOYMENT_HOST", "$DEPLOYMENT_HOST_CREDENTIALS") {
            docker.withRegistry("$PRIVATE_REGISTRY" , 'artifactory') {

              //echo 'Stop and remove the specified Docker containers from the DEV server'
              sh 'sh delete_hetzner.sh'
            }
          }
        }
      }
    }
  }
}

