/**
* Jenkinsfile to deploy multiple Docker containers based on docker-compose into a DEV server and run any test.
* This pipeline will run the Docker containers, execute the tests and then stop and remove the containers from the DEV  
* server automatically.
**/


pipeline {

  agent any

    environment {
      
      APP_URL = "https://kea.curex-project.eu"
      KEYCLOAK_URL = "https://keycloak.curex-project.eu/auth/realms/master/protocol/openid-connect/token"
      USER = credentials('keycloak-test-user')
      PASSWORD = credentials('keycloak-test-password')
      CLIENT_ID = "KEA"

    }

  stages {
    
    stage('Run tests') {
      steps {
        script {
          echo '*************'
          echo '*** TESTS ***'
          echo '*************'
          /* Here do your tests */
          try {
            // Get Keycloak auth token
            String token = sh(label: "get_keycloak_token", script: "curl -s -X POST $KEYCLOAK_URL -H 'Content-Type: application/x-www-form-urlencoded' -d 'username=$USER' -d 'password=$PASSWORD' -d 'grant_type=password'  -d 'client_id=$CLIENT_ID' | sed 's/.*access_token\":\"//g' | sed 's/\".*//g'", returnStdout: true)

            // MLTD
            String testName = "KEA_TIE_I001"

            String url = "$APP_URL/api/v1/mltd/start"
            String responseCode = sh(label: testName, script: "curl -m 10 -sL -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json, text/plain, */*' $url", returnStdout: true)
            echo responseCode
            if (!responseCode.endsWith('200')) {
              error("$testName: Returned status code = $responseCode when calling $url")
            }
            String processId = responseCode.split(",")[1].split(":")[1];

            sleep 5;

            url = "$APP_URL/api/v1/xlsiem"
            responseCode = sh(label: testName, script: "curl -m 10 -sL -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json' -H 'Content-Type: application/json' --data-raw \"`cat pcap-data/mltd1-unix.json`\" $url -o /dev/null", returnStdout: true)
            echo responseCode

            sleep 5;

            if (responseCode != '200') {
              error("$testName: Returned status code = $responseCode when calling $url")
            }
            responseCode = sh(label: testName, script: "curl -m 10 -sL -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json' -H 'Content-Type: application/json' --data-raw \"`cat pcap-data/mltd2-unix.json`\" $url -o /dev/null", returnStdout: true)
            echo responseCode
            if (responseCode != '200') {
              error("$testName: Returned status code = $responseCode when calling $url")
            }

            sleep 5;

            url = "$APP_URL/api/v1/mltd/stop/$processId"
            responseCode = sh(label: testName, script: "curl -m 10 -sLI -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json, text/plain, */*' $url -o /dev/null", returnStdout: true)
            echo responseCode
            if (responseCode != '200') {
              error("$testName: Returned status code = $responseCode when calling $url")
            }

            // OD
            testName = "KEA_TIE_I002"

            url = "$APP_URL/api/v1/od/start"
            responseCode = sh(label: testName, script: "curl -m 10 -sL -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json, text/plain, */*' $url", returnStdout: true)
            echo responseCode
            if (!responseCode.endsWith('200')) {
              error("$testName: Returned status code = $responseCode when calling $url")
            }
            processId = responseCode.split(",")[1].split(":")[1].split('"')[1];
            url = "$APP_URL/api/v1/od/analyze/$processId"
            responseCode = sh(label: testName, script: "curl -m 10 -sL -w '%{http_code}' -H 'Authorization: Bearer $token' -F file=@pcap-data/big.pcap $url -o /dev/null", returnStdout: true)
            echo responseCode
            if (responseCode != '200') {
              error("$testName: Returned status code = $responseCode when calling $url")
            }
            url = "$APP_URL/api/v1/od/stop/$processId"
            responseCode = sh(label: testName, script: "curl -m 10 -sLI -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json, text/plain, */*' $url -o /dev/null", returnStdout: true)
            echo responseCode
            if (responseCode != '200') {
              error("$testName: Returned status code = $responseCode when calling $url")
            }
            
          } catch (ignored) {
            currentBuild.result = 'FAILURE'
            echo "KEA Deployment Tests failed"
          }
        }
      }
    }

  }
}

