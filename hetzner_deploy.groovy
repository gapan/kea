/**
* Jenkinsfile to deploy multiple Docker containers based on docker-compose into a DEV server and run any test.
* This pipeline will run the Docker containers, execute the tests and then stop and remove the containers from the DEV  
* server automatically.
**/


pipeline {

  agent any

    environment {
      // Keep as is the following two lines
      PRIVATE_REGISTRY = "https://registry.curex-project.eu:443/curex-local/"
      ARTIFACTORY_URL = "registry.curex-project.eu:443/curex-local/"

      HOST_IP = "116.203.166.220"
      DEPLOYMENT_HOST = "${HOST_IP}:2376"
      DEPLOYMENT_HOST_CREDENTIALS = "vm2-creds"
      
      // KEA specific
      APP_NAME = "KEA"
      APP_ENV = "production"
      APP_DEBUG = "true"
      APP_URL = "https://kea.curex-project.eu"

      KIBANA_URL = "https://kea-kibana.vlahavas.com"
      GRAFANA_URL = "https://kea-grafana.vlahavas.com"
      KIBANA_PORT = "null"
      GRAFANA_PORT = "null"
      
      LOG_CHANNEL = "stack"
      
      DB_CONNECTION = "pgsql"
      DB_HOST = "timescaledb"
      DB_PORT = "5432"
      DB_DATABASE = "kea"
      DB_USERNAME = "postgres"
      DB_PASSWORD = "postgres"
      
      BROADCAST_DRIVER = "log"
      CACHE_DRIVER = "file"
      QUEUE_CONNECTION = "sync"
      SESSION_DRIVER = "file"
      SESSION_LIFETIME = "120"
      
      JWT_TTL = "1440"
      
      ELASTICSEARCH_HOST = "elasticsearch"
      ELASTICSEARCH_PORT = "9200"
      ELASTICSEARCH_SCHEME = "http"
      
      MQTT_HOST = "mosquitto"
      MQTT_PORT = "1883"
      MQTT_DEBUG = "false"
      MQTT_QOS = "0"
      MQTT_RETAIN = "0"
      
      MLTD_HOST = "mltd"
      MLTD_PORT = "5000"
      OD_HOST = "od"
      OD_PORT = "9091"
      
      KEYCLOAK_REALM_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh0kSAG2A/h7VvxCcZK1LyfJFSNTO7ycspaVkwjzu8+6Y32uVCVZro0Bdp2CUhEDlQgHN6NtWG/qdc4iUIENFWPLa8BOTrDX5YeM5oSK1jJuhd1dtlMZok2tLvVRy/VDWm65vFUwfycAbfiivdK5yZm/lUyf8jPAvlhEKWhi/BWgBTLJEQ4tHxoXOqrfCgP3KHCcyhstLRQ+V4KocdhiU9Piosg/iL0zOgm0bA74CVyM7by9kMbC4orPt3M185N5z67iuPrbBtekeMyhHby72kP3HvYIhuNkV1PqdjelonjrzmRkScYafxeoQ8Ec7CuLtEt9k7BbWWUxj/OB05kixywIDAQAB"
      KEYCLOAK_LOAD_USER_FROM_DATABASE = "false"
      KEYCLOAK_USER_PROVIDER_CREDENTIAL = "name"
      KEYCLOAK_TOKEN_PRINCIPAL_ATTRIBUTE = "preferred_username"
      KEYCLOAK_APPEND_DECODED_TOKEN = "false"
      KEYCLOAK_ALLOWED_RESOURCES = "account"
      KEYCLOAK_REALM = "master"
      KEYCLOAK_URL = "https://keycloak.curex-project.eu/auth/"
      KEYCLOAK_CLIENT_ID = "KEA"

      AUTH_ENABLED = "true"

      OD_GRAFANA_PARAMS = "/d/qDDyJZ6Wz/od?orgId=1"
      MLTD_GRAFANA_PARAMS = "/d/dtCrgk6Wk/mltd?orgId=1"
      CEPTD_KIBANA_PARAMS = "/app/kibana#/dashboard/Suricata?_g=(refreshInterval%3A(pause%3A!t%2Cvalue%3A0)%2Ctime%3A(from%3Anow-15m%2Cto%3Anow))"
      
      RSYSLOG_SERVER = credentials('rsyslog-ip')
      RSYSLOG_PORT = credentials('rsyslog-port')
    }

  stages {
           
    stage('Checkout the source code') {
      steps {
        checkout scm
      }
    }
    
  
    stage('Deploy Docker containers in DEV server') {
      steps {
        script {
          docker.withServer("$DEPLOYMENT_HOST", "$DEPLOYMENT_HOST_CREDENTIALS") {
            docker.withRegistry("$PRIVATE_REGISTRY" , 'artifactory') {
              echo 'Deploying the specified Docker containers in DEV server'
              sh 'sh deploy_hetzner.sh'
            }
          }
        }
      }
    }
  }
}

