# LGPLv3 Copyright (C) 2022 ATOS
# Jesús Villalobos Nieto

#############################################
####### XL-SIEM RabbitMQ Client #############
#############################################

This RabbitMQ client is used to consume the XL-SIEM outputs: Events and Alarms.

First, install the python dependencies from the "requirements.txt" file:
$> pip install -r requirements.txt

The configuration file has been pre-configured, you can modify it by convenience:
$> vi reader.config

Add your own code to the main python script "xlsiem_rabbitmq_client.py" in the tagged section.

Execute the python script:
$> python xlsiem_rabbitmq_client.py