import json
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import base64
import re

log_file = open("/home/thanasis/Downloads/events_fphag.log","r")
events_list = []
for line in log_file.readlines():
    jsonObj = json.loads(line.replace('"','\\"').replace("'message'",'"message"').replace("'",'"'))
    message = json.loads(jsonObj["message"])
    event = message["event"]
    event_body = {}
    log = base64.b64decode(event["log"]).decode('utf8')
    m = re.search(r'\[Classification\:\s(.+)\]\s\[', log)
    event_body["event_alarm_id"] = m.group(1)
    event_body["asset_id"] = event["organization"]
    event_body["timestamp"] = int(event["date"])
    # event_body["event_alarm_id"] = event["event_id"]
    event_body["event_alarm_char"] = event["type"]
    event_body["name"] = event["type"]
    event_body["source_ip"] = event["src_ip"]
    event_body["source_port"] = int(event["src_port"])
    event_body["destination_ip"] = event["dst_ip"]
    event_body["dst_port"] = int(event["dst_port"])
    events_list.append(event_body)
df = pd.DataFrame.from_dict(events_list)
df["timestamp"] = pd.to_datetime(df["timestamp"])
df.set_index(pd.DatetimeIndex(df['timestamp']))
df.plot(y='dst_port')
plt.show()

a_df=np.unique(df[['asset_id']], axis=0)