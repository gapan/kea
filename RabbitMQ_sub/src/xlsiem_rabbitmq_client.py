__author__ = "Jesus Villalobos, Ruben Trapero"
__copyright__ = "Copyright (C) 2020 ATOS"
__license__ = "LGPLv3"
__version__ = "1.0"

import atexit
import json
import re
import base64
import requests
import time
from messageReader import MessageReader
from multiprocessing import Process

def callback(ch, method, properties, body):
    global first_message
    global unique_events
    global access_token
    try:
        event_dict = json.loads(body)
        date = json.loads(event_dict["message"])["event"]["date"]
        src_ip = json.loads(event_dict["message"])["event"]["src_ip"]
        src_port = json.loads(event_dict["message"])["event"]["src_port"]
        dst_ip = json.loads(event_dict["message"])["event"]["dst_ip"]
        dst_port = json.loads(event_dict["message"])["event"]["dst_port"]
        organization = json.loads(event_dict["message"])["event"]["organization"]
        log = json.loads(event_dict["message"])["event"]["log"]
        base64_bytes = log.encode('ascii')
        message_bytes = base64.b64decode(base64_bytes)
        log = message_bytes.decode('ascii')
        classification_match = re.match(r".*\[\*\*\]\s\[Classification:\s(.*)\]\s\[", log)
        if classification_match:
            event = classification_match.group(1)

        message = {"asset_id": "server",
            "timestamp": int(date),
            "event_alarm": [
                {"event_alarm_id": event, "event_alarm_char": event,
                    "name": "xlsiem_event", "source_ip": src_ip,
                    "source_port": int(src_port), "destination_ip": dst_ip,
                    "destination_port": int(dst_port), "priority": 0, "confidence": 0, }], }


        if len(first_message) == 0:
            first_message = message
            unique_events.add(event)
        else:
            if message["timestamp"]-first_message["timestamp"] < 10:
                if event not in unique_events:
                    first_message["event_alarm"] += message["event_alarm"]
                    unique_events.add(event)
            else:
                print(unique_events)
                url = "http://webserver/api/v1/xlsiem"
                headers = {'Content-Type': 'application/json','Authorization': f'Bearer {access_token}'}
                payload = json.dumps(first_message)
                response = requests.request("POST", url, headers=headers, data=payload)
                print(f"Event sent to KEA: {response}")
                first_message = {}
                unique_events = set()
        # ch.basic_ack(delivery_tag=method.delivery_tag)

    except Exception as e:
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        authBody = {
            "username": "testUser",
            "password": "73T$a2W3ght!8X1721R5",
            "grant_type": "password",
            "client_id": "KEA"
        }
        url = "https://keycloak.fphag.curex-project.eu/auth/realms/master/protocol/openid-connect/token"
        response = requests.request("POST", url, headers=headers, data=authBody)
        access_token = json.loads(response.text)['access_token']
        print(e)


def clean():
    reader.clean()

def cleaner():
    while True:
        print('cleaner: starting')
        global first_message
        global unique_events
        if len(first_message)>0:
            url = "http://webserver/api/v1/xlsiem"
            headers = {'Content-Type': 'application/json'}
            payload = json.dumps(first_message)
            response = requests.request("POST", url, headers=headers, data=payload)
            print(f"Event sent to KEA: {response}")
            first_message = {}
            unique_events = set()
        print('cleaner: finished')
        time.sleep(60)

unique_events = set()
first_message = {}

#p2 = Process(target=cleaner)
#p2.start()

# time.sleep(30)
# with open("events.log","r") as f:
#      for line in f.readlines():
#          callback(None,None,None,line)

headers = {'Content-Type': 'application/x-www-form-urlencoded'}
authBody = {
    "username": "testUser",
    "password": "73T$a2W3ght!8X1721R5",
    "grant_type": "password",
    "client_id": "KEA"
}
url = "https://keycloak.fphag.curex-project.eu/auth/realms/master/protocol/openid-connect/token"
response = requests.request("POST", url, headers=headers, data=authBody)
access_token = json.loads(response.text)['access_token']

reader = MessageReader()
reader.connect()
reader.read(callback)
atexit.register(clean)

