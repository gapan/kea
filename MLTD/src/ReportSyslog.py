import logging.handlers

def report(asset_id, risk, timeframe, source_ip, target_ip):
    my_logger = logging.getLogger('MyLogger')
    my_logger.setLevel(logging.DEBUG)

    # handler = logging.FileHandler('mltd.log')
    handler = logging.handlers.SysLogHandler(address='/dev/log')
    handler.ident = 'KEA-MLTD'
    formatter = logging.Formatter(' %(message)s')
    handler.setFormatter(formatter)
    my_logger.addHandler(handler)
    my_logger.critical(f'source_ip: {source_ip} target_ip: {target_ip} risk: {risk} incident: Bruteforce Attack is Detected')


