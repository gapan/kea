#!/bin/bash

sed -i "s/RSYSLOG_SERVER/$RSYSLOG_SERVER/g"  /etc/rsyslog.d/xlsiem.conf
sed -i "s/RSYSLOG_PORT/$RSYSLOG_PORT/g"  /etc/rsyslog.d/xlsiem.conf
service rsyslog restart

python3 MLTD_API.py
