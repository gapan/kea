/**
* Jenkinsfile to deploy multiple Docker containers based on docker-compose into a DEV server and run any test.
* This pipeline will run the Docker containers, execute the tests and then stop and remove the containers from the DEV  
* server automatically.
**/


pipeline {

  agent any

  environment {

    // Keep as is the following two lines
    PRIVATE_REGISTRY = "https://registry.curex-project.eu:443/curex-local/"
    ARTIFACTORY_URL = "registry.curex-project.eu:443/curex-local/"

    HOST_IP = "116.203.166.220"
    DEPLOYMENT_HOST = "${HOST_IP}:2376"
    DEPLOYMENT_HOST_CREDENTIALS = "vm2-creds"
  }

  stages {
           
    stage('Anchore Analysis') {  
      steps {  
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_suricata:latest" > anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_elasticsearch:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_logstash:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_kibana:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_webserver:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_api:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_od:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_mltd:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_mqtt:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_timescaledb:latest" >> anchore_images'
        sh 'echo "registry.curex-project.eu:443/curex-local/kea_grafana:latest" >> anchore_images'
        anchore name: 'anchore_images', bailOnFail: true, annotations: [[key: 'AUTH', value: 'by Jenkins']], engineRetries: '6000', policyBundleId: '2c53a13c-1765-11e8-82ef-23527761d060'
      }  
    } 
  }
}

