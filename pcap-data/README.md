# PCAP and JSON data files

These are some PCAP files with network traffic that can be used to test
KEA functionality, along with JSON files for submitting to the different
modules of KEA for testing.
