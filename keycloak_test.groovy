/**
* Jenkinsfile to run keycloak tests on a keycloak enabled app.
* An instance of the application should already be deployed. Two tests
* are included, one that tries to access an API endpoint without any
* authentication and should fail and a second one that uses
* authentication and should succeed.
**/


pipeline {

  agent any

    environment {
      
      APP_URL = "https://kea.curex-project.eu"
      KEYCLOAK_URL = "https://keycloak.curex-project.eu/auth/realms/master/protocol/openid-connect/token"
      USER = credentials('keycloak-test-user')
      PASSWORD = credentials('keycloak-test-password')
      CLIENT_ID = "KEA"

    }

  stages {
           
    stage('Run tests') {
      steps {
        script {
          echo '*************'
          echo '*** TESTS ***'
          echo '*************'
          /* Here do your tests */
          try {
            String testName = "KEA_keycloak_fail"
            String url = "$APP_URL/api/v1/od/status"
            String responseCode = sh(label: testName, script: "curl -m 10 -sLI -w '%{http_code}' -H 'Accept: application/json, text/plain, */*' $url -o /dev/null", returnStdout: true)
            if ( responseCode != '401' ) {
              error("$testName: Returned status code = $responseCode when calling $url")
            }

            testName = "KEA_keycloak_success"
            String token = sh(label: "get_keycloak_token", script: "curl -s -X POST $KEYCLOAK_URL -H 'Content-Type: application/x-www-form-urlencoded' -d 'username=$USER' -d 'password=$PASSWORD' -d 'grant_type=password'  -d 'client_id=$CLIENT_ID' | sed 's/.*access_token\":\"//g' | sed 's/\".*//g'", returnStdout: true)
            responseCode = sh(label: testName, script: "curl -m 10 -sLI -w '%{http_code}' -H 'Authorization: Bearer $token' -H 'Accept: application/json, text/plain, */*' $url -o /dev/null", returnStdout: true)
            if ( responseCode != '200' ) {
              error("$testName: Returned status code = $responseCode when calling $url")
            }
          } catch (ignored) {
            currentBuild.result = 'FAILURE'
            echo "KEA Keycloak Tests failed"
          }
        }
      }
    }
  }
}

