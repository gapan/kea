#!/bin/sh

if [ "$1" == ONLINE ]; then
	suricata -v -i $(/sbin/ip address | grep '^2: ' | awk '{ print $2 }' | tr -d [:punct:]) &
else
	suricata -v -r /var/pcap &
fi

process_suricata_log.py /opt/capec/capecdb.sqlite /var/log/suricata/eve.json /var/log/suricata/capec.json
