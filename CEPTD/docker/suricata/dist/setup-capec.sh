#!/bin/sh

# Adapting from https://github.com/dtag-dev-sec/listbot
function processMap {
  ruleCount=$(wc -l < $1)
  # Just extract rules with CVE ID, for proper matching we also need SID
  let i=0
  let j=0
  while read -r sRule
  do
    i=$(( $i + 1 ))
    echo -ne "Processing rule ($i / $ruleCount)\r"
    cve=$(echo $sRule | grep -o -E "(cve,|CVE-|CAN-)([0-9]{4}-([0-9]{4}|[0-9]{5}))" | tr a-z A-Z | tr ",|-" " " | awk '{ print $1"-"$2"-"$3 }')
    if [ "$cve" != "" ]; then
      sid=$(echo $sRule | awk '{ print $1 }')
      echo $sid $cve >> $2
      j=$(( j + 1 ))
    fi
  done < "$1"
}

# URLs used for fetching
capecUrl="https://capec.mitre.org/data/xml/views/1000.xml.zip"
cweUrl="https://cwe.mitre.org/data/xml/views/1000.xml.zip"
cveUrl="https://cve.mitre.org/data/downloads/allitems.xml"
rulesMapping="http://rules.emergingthreatspro.com/open/suricata-5.0/rules/sid-msg.map"
dbfiles=/opt/capec

# Check to connection mitre.org
wget -q --spider https://mitre.org
mitreCon=$?

# Check connection to ET
wget -q --spider https://rules.emergingthreatspro.com
etCon=$?

if [ $mitreCon -eq 0 ] && [ $etCon -eq 0 ]; then
    echo "Downloading CAPEC..."
    wget ${capecUrl} -O /tmp/capec.xml.zip 2>&1 > /dev/null
    echo "Extracting CAPEC..."
    unzip -d /tmp /tmp/capec.xml.zip; mv /tmp/1000.xml ${dbfiles}/capec.xml

    echo "Downloading CWE..."
    wget ${cweUrl} -O /tmp/cwe.xml.zip 2>&1 > /dev/null
    echo "Extracting CWE..."
    unzip -d /tmp /tmp/cwe.xml.zip; mv /tmp/1000.xml ${dbfiles}/cwe.xml

    #echo "Downloading CVE..."
    #wget ${cveUrl} -O /tmp/cve.xml 2>&1 > /dev/null

    echo "Downloading Suricata to CVE mapping..."
    wget ${rulesMapping} -O /tmp/sid-msg.map 2>&1 > /dev/null
    processMap /tmp/sid-msg.map ${dbfiles}/sid-cve

    echo "Constructing CAPEC DB..."
    create_capec_db.py ${dbfiles} ${dbfiles}/capecdb.sqlite
else
    echo "No connection to fetch data, exiting..."
    exit 1
fi
