#!/bin/sh

# Basic ruleset used by Suricata, provided by Emerging Threats
etUrl="https://rules.emergingthreatspro.com/open/suricata-5.0/emerging.rules.tar.gz"

# Set Capture Filters
# https://suricata.readthedocs.io/en/latest/performance/ignoring-traffic.html#capture-filters-bpf

# Check connection to Emerging Threats
wget -q --spider https://rules.emergingthreatspro.com

if [ $? -eq 0 ]; then
    wget ${etUrl} -O /tmp/rules.tar.gz 2>&1 > /dev/null
    tar xvfz /tmp/rules.tar.gz -C /etc/suricata/  2>&1 > /dev/null
    sed -i s/^#alert/alert/ /etc/suricata/rules/*.rules 2>&1 > /dev/null
else
    echo "No connection to ET, exiting..."
    exit 1
fi
