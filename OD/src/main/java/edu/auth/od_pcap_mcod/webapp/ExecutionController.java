package edu.auth.od_pcap_mcod.webapp;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.auth.od_pcap_mcod.model.Data;
import edu.auth.od_pcap_mcod.model.Packet;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.auth.od_pcap_mcod.report.ReportManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ExecutionController {
    
    public static int arrivalTime = 0;
    public static Map<String,Execution> threadsMap = new HashMap();
    public static DetectionAPIParameters detectionAPIparamenter = null;

    @RequestMapping(value = "/api/v1/od")
    public ResponseEntity<Map> execute(@RequestBody DetectionAPIParameters params) {
        Execution e  = new Execution(params);
        String id = UUID.randomUUID().toString();
        threadsMap.put(id,e);
        HashMap<String,String> pid = new HashMap<String,String>();
        pid.put("pid",id);
        return ResponseEntity.ok(pid);
    }
    
    @RequestMapping(value = "/api/v1/od/analyse/{process_id}",
			method = RequestMethod.POST,
			consumes = MediaType.MULTIPART_FORM_DATA_VALUE)

	public ResponseEntity<Map> fileUpload(@RequestParam("file") MultipartFile file, @PathVariable("process_id") String process_id) throws IOException
	{
        Logger logger = Logger.getLogger(ReportManager.class.getName());
        logger.setLevel(Level.INFO);
	    if (threadsMap.containsKey(process_id)) {
            Execution e = threadsMap.get(process_id);
            File convertFile = new File(file.getOriginalFilename());
            boolean newFile = convertFile.createNewFile();
            if (!newFile){
                logger.fine("File Already Exists");
            }

            try (FileOutputStream fout = new FileOutputStream(convertFile)) {
                fout.write(file.getBytes());
            } catch (Exception exe) {
                exe.printStackTrace();
            }

            HashMap<String, TreeMap<Integer, List<Packet>>> sendPackets = getTCPpackets(convertFile.getAbsolutePath());
            HashMap<String, TreeMap<Integer, List<Packet>>> retransPackets = getTCPpacketsRetrans(convertFile.getAbsolutePath());

            for(String packetIP : sendPackets.keySet()){
                if(retransPackets.containsKey(packetIP)){
                    TreeMap<Integer, List<Packet>> tcpPacketRetransTimes = retransPackets.get(packetIP);
                    TreeMap<Integer, List<Packet>> tcpPacketTimes = sendPackets.get(packetIP);
                    int startTime = tcpPacketTimes.firstKey();
                    int endTime = tcpPacketTimes.lastKey();

                    List<Data> slideDataList = new ArrayList<Data>();

                    for (int i = startTime; i <= endTime; i++) {
                        double tcpPacketsCount = 0;
                        double tcpRetransCount = 0;
                        arrivalTime += 1;
                        if (tcpPacketTimes.containsKey(i)) {
                            tcpPacketsCount = tcpPacketTimes.get(i).size();
                            if (tcpPacketRetransTimes.containsKey(i)) {
                                tcpRetransCount = tcpPacketRetransTimes.get(i).size();
                            }
                        }
                        if (tcpRetransCount == 0) {
                            slideDataList.add(new Data(arrivalTime, Instant.now(), new Packet(-1, Instant.now(), -1, "", packetIP), 0.0));
                        } else if (tcpPacketsCount == 0) {
                            slideDataList.add(new Data(arrivalTime, tcpPacketRetransTimes.get(i).get(0).getArrivalTime(), tcpPacketRetransTimes.get(i).get(0), 1.0));
                        } else {
                            double ratio = tcpRetransCount / tcpPacketsCount;
                            slideDataList.add(new Data(arrivalTime, tcpPacketTimes.get(i).get(0).getArrivalTime(), tcpPacketRetransTimes.get(i).get(0), ratio));
                        }
                    }
                    e.execute(slideDataList, arrivalTime);
                }
            }
            return ResponseEntity.ok(new HashMap());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

    
    @RequestMapping(value="/api/v1/od/status/{process_id}", method=RequestMethod.GET)
    public ResponseEntity<Map> isRunningPrediction(@PathVariable("process_id") String process_id) {
        Map json = new HashMap();
        json.put("is_running", threadsMap.containsKey(process_id));
        return ResponseEntity.ok(json);
    }
    
    @GetMapping("/api/v1/od/status")
    public ResponseEntity<Map> getRunningPredictions(){
        Map json = new HashMap();
        for(String e : threadsMap.keySet()){
            json.put(e,"/api/v1/od/stop/"+e);
        }
        return ResponseEntity.ok(json);
    }

    @RequestMapping(value="/api/v1/od/stop/{process_id}", method=RequestMethod.GET)
    public ResponseEntity<Map> stop(@PathVariable("process_id") String process_id){
        if(threadsMap.containsKey(process_id)){
            threadsMap.get(process_id).interrupt();
        }
        threadsMap.remove(process_id);
        Map json = new HashMap();
        json.put("Running threads", threadsMap.size());
        return ResponseEntity.ok(json);
    }

    @GetMapping("/api/v1/od/stopall")
    public ResponseEntity<Map> stopAll(){
        for(String e : threadsMap.keySet()){
            threadsMap.get(e).interrupt();
        }
        threadsMap.clear();
        Map json = new HashMap();
        json.put("Running threads", threadsMap.size());
        return ResponseEntity.ok(json);
    }
    
    @GetMapping("/api/v1/od/getDefaultParams")
    public DetectionAPIParameters defaults(){
        if(detectionAPIparamenter == null){
            return new DetectionAPIParameters();
        } else {
            return detectionAPIparamenter;
        }
    }
     
    @RequestMapping("/api/v1/od/saveParams")
    public ResponseEntity<Map> saveParams(@RequestBody DetectionAPIParameters params) {
        detectionAPIparamenter = params;
        return ResponseEntity.ok(new HashMap());
    }
    
    private HashMap<String, TreeMap<Integer,List<Packet>>> getTCPpackets(String pcapFilePath) throws IOException {
        HashMap<String, TreeMap<Integer,List<Packet>>> sendPackets = new HashMap();
        Runtime rt = Runtime.getRuntime();
        String[] commands = {"/usr/bin/tshark", "-r", pcapFilePath, "-T", "fields", "-e", "frame.number", "-e", "frame.time_relative", "-e", "frame.time",  "-e", "ip.addr", "-E", "separator=;", "tcp"};
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String s = null;
        while ((s = stdInput.readLine()) != null) {
            String[] sSplit = s.split(";");
            int id = Integer.parseInt(sSplit[0]);
            double roundedTime = Double.parseDouble(sSplit[1]);
            String arrivalTime = sSplit[2];
            String ipSourceTarget = sSplit[3];
            if (ipSourceTarget == null || ipSourceTarget == ""){
                continue;
            }
            String sourceIP = ipSourceTarget.split(",")[0];
            String targetIP = ipSourceTarget.split(",")[1];
            Packet pac = new Packet(id, arrivalTime, roundedTime, sourceIP, targetIP);

            TreeMap<Integer,List<Packet>> tcpPacketTimes = null;
            if (!sendPackets.containsKey(pac.getPacketIP())){
                sendPackets.put(pac.getPacketIP(), new TreeMap<Integer,List<Packet>>());
            }
            tcpPacketTimes = sendPackets.get(pac.getPacketIP());

            if(!tcpPacketTimes.containsKey(pac.getRelativeArrivalTime())){
                tcpPacketTimes.put(pac.getRelativeArrivalTime(),new ArrayList<Packet>());
            } 
            tcpPacketTimes.get(pac.getRelativeArrivalTime()).add(pac);
        }
        return sendPackets;
    }
    
    private HashMap<String, TreeMap<Integer,List<Packet>>> getTCPpacketsRetrans(String pcapFilePath) throws IOException {
        HashMap<String, TreeMap<Integer,List<Packet>>> retransPackets = new HashMap();
        Runtime rt = Runtime.getRuntime();
        String[] commands = {"/usr/bin/tshark", "-r", pcapFilePath, "-T", "fields", "-e", "frame.number", "-e", "frame.time_relative", "-e", "frame.time", "-e", "ip.addr", "-Y", "tcp.analysis.retransmission", "-E", "separator=;"};
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));

        String s = null;
        while ((s = stdInput.readLine()) != null) {
            String[] sSplit = s.split(";");
            int id = Integer.parseInt(sSplit[0]);
            double roundedTime = Double.parseDouble(sSplit[1]);
            String arrivalTime = sSplit[2];
            String ipSourceTarget = sSplit[3];
            if (ipSourceTarget == null || ipSourceTarget == ""){
                continue;
            }
            String sourceIP = ipSourceTarget.split(",")[0];
            String targetIP = ipSourceTarget.split(",")[1];
            Packet pac = new Packet(id, arrivalTime, roundedTime, sourceIP, targetIP);

            TreeMap<Integer,List<Packet>> tcpPacketTimes = null;
            if (!retransPackets.containsKey(pac.getPacketIP())){
                retransPackets.put(pac.getPacketIP(), new TreeMap<Integer,List<Packet>>());
            }
            tcpPacketTimes = retransPackets.get(pac.getPacketIP());

            if(!tcpPacketTimes.containsKey(pac.getRelativeArrivalTime())){
                tcpPacketTimes.put(pac.getRelativeArrivalTime(),new ArrayList<Packet>());
            }
            tcpPacketTimes.get(pac.getRelativeArrivalTime()).add(pac);
        }
        return retransPackets;
    }
}

