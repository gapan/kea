/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.auth.od_pcap_mcod.webapp;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author thanasis
 */
public class DetectionAPIParameters implements Serializable{
    @JsonProperty("r")
    private String R = "0.1";
    
    @JsonProperty("w")
    private String W = "60";
    
    @JsonProperty("k")
    private String k = "20";
    
    @JsonProperty("slide")
    private String slide = "10";
    
    @JsonProperty("outlier_life")
    private String outlierLife = "0";
    
    @JsonProperty("measurement")
    private String meas = "oillevel";
        
    @JsonProperty("mqtt_host")
    private String mqttHost = "mqtt-broker";
    
    @JsonProperty("mqtt_port")
    private String mqttPort = "1883";
    
    @JsonProperty("mqtt_topic")
    private String topic = "hot-forming-press/meas";
    
    @JsonProperty("mqtt_usermane")
    private String mqttUsername = "";
    
    @JsonProperty("mqtt_password")
    private String mqttPassword = "";
    
    @JsonProperty("timeDb_host")
    private String timeDbHost = "https://localhost";
    
    @JsonProperty("timeDb_port")
    private String timeDbPort = "8086";
    
    @JsonProperty("timeDb_ssl")
    private String timeDbSsl = "true";
    
    @JsonProperty("timeDb_database")
    private String resutlsDB = "Axoom1_2019";
    
    @JsonProperty("timeDb_table")
    private String resultsTable = "Detected_failures";
    
    @JsonProperty("timeDb_username")
    private String timeDbUsername = "";
    
    @JsonProperty("timeDb_password")
    private String timeDbPassword= "";

    @JsonProperty("rsyslog_server")
    private String rsyslogServer = "";

    @JsonProperty("rsyslog_port")
    private int rsyslogPort = 514;

    public String getOutlierLife() {
        return outlierLife;
    }

    public void setOutlierLife(String outlierLife) {
        this.outlierLife = outlierLife;
    }

    public String getTimeDbSsl() {
        return timeDbSsl;
    }

    public void setTimeDbSsl(String timeDbSsl) {
        this.timeDbSsl = timeDbSsl;
    }    

    public String getR() {
        return R;
    }

    public void setR(String R) {
        this.R = R;
    }

    public String getW() {
        return W;
    }

    public void setW(String W) {
        this.W = W;
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getSlide() {
        return slide;
    }

    public void setSlide(String slide) {
        this.slide = slide;
    }
    
    public String getMqttHost() {
        return mqttHost;
    }

    public void setMqttHost(String mqttHost) {
        this.mqttHost = mqttHost;
    }

    public String getMqttPort() {
        return mqttPort;
    }

    public void setMqttPort(String mqttPort) {
        this.mqttPort = mqttPort;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMqttUsername() {
        return mqttUsername;
    }

    public void setMqttUsername(String mqttUsername) {
        this.mqttUsername = mqttUsername;
    }

    public String getMqttPassword() {
        return mqttPassword;
    }

    public void setMqttPassword(String mqttPassword) {
        this.mqttPassword = mqttPassword;
    }

    public String getMeas() {
        return meas;
    }

    public void setMeas(String meas) {
        this.meas = meas;
    }

    public String getTimeDbHost() {
        return timeDbHost;
    }

    public void setTimeDbHost(String timeDbHost) {
        this.timeDbHost = timeDbHost;
    }

    public String getTimeDbPort() {
        return timeDbPort;
    }

    public void setTimeDbPort(String timeDbPort) {
        this.timeDbPort = timeDbPort;
    }

    public String getResutlsDB() {
        return resutlsDB;
    }

    public void setResutlsDB(String resutlsDB) {
        this.resutlsDB = resutlsDB;
    }

    public String getResultsTable() {
        return resultsTable;
    }

    public void setResultsTable(String resultsTable) {
        this.resultsTable = resultsTable;
    }

    public String getTimeDbUsername() {
        return timeDbUsername;
    }

    public void setTimeDbUsername(String timeDbUsername) {
        this.timeDbUsername = timeDbUsername;
    }

    public String getTimeDbPassword() {
        return timeDbPassword;
    }

    public void setTimeDbPassword(String timeDbPassword) {
        this.timeDbPassword = timeDbPassword;
    }

    public String getRsyslogServer() {
        return rsyslogServer;
    }

    public void setRsyslogServer(String rsyslogServer) {
        this.rsyslogServer = rsyslogServer;
    }


    public int getRsyslogPort() {
        return rsyslogPort;
    }

    public void setRsyslogPort(int rsyslogPort) {
        this.rsyslogPort = rsyslogPort;
    }
}
