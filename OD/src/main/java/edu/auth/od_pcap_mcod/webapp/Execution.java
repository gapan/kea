package edu.auth.od_pcap_mcod.webapp;

import edu.auth.od_pcap_mcod.mcod.MCOD;
import edu.auth.od_pcap_mcod.model.Data;
import edu.auth.od_pcap_mcod.model.Packet;
import edu.auth.od_pcap_mcod.report.ReportManager;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author thanasis
 */
public class Execution extends Thread{

    private MCOD mcod;
    private HashMap<String, MCOD> mcoders;
    private ReportManager rman;
    private double R;
    private int k;
    private int W;
    private int slide;
    private double outlierLife;

    public Execution(DetectionAPIParameters params) {
        
        this.R = Double.parseDouble(params.getR());
        this.k = Integer.parseInt(params.getK());
        this.W = Integer.parseInt(params.getW());
        this.slide = Integer.parseInt(params.getSlide());
        this.outlierLife = Double.parseDouble(params.getOutlierLife());

        this.rman = new ReportManager(params);
        mcoders = new HashMap<String, MCOD>();
    }
    
    public void execute(List<Data> slideDataList, int currentTime){
        String mcoderId = slideDataList.get(0).getPacketIP();
        if(!mcoders.containsKey(mcoderId)){
            mcoders.put(mcoderId, new MCOD(this.R, this.k, this.W, this.slide));
        }
        mcodOutliers(mcoders.get(mcoderId), slideDataList, currentTime, mcoderId);
    }
    
    private void mcodOutliers(MCOD mcnew, List<Data> slideDataList, long currentTime, String mcoderId) {
        long startTime = System.nanoTime();
        ArrayList<Data> outliers9 = mcnew.detectOutlier(slideDataList, currentTime, W, slide);
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;
        for (Data outlier : outliers9) {
            outlier.setPacket(new Packet(-1, Instant.now(), -1, "", mcoderId));
            double outlierLifeInSlides = 0;
            if (outlier.getOutlier_end_time() != 0) {
                outlierLifeInSlides = (outlier.getOutlier_end_time() / Double.valueOf(slide)) - (outlier.getOutlier_start_time() / Double.valueOf(slide)) + 1.0;
            } else {
                outlierLifeInSlides = (currentTime / Double.valueOf(slide)) - (outlier.getOutlier_start_time() / Double.valueOf(slide)) + 1.0;
            }
            if ((outlierLifeInSlides >= ((double) W / slide) * outlierLife) && (outlier.getArrivalTime() > currentTime - W)) {
                rman.reportOutlier(outlier, "network");
            }
        }
    }

    @Override
    public void run() {
        super.run();
    }
}
