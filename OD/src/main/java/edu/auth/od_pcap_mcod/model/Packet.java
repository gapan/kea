/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.auth.od_pcap_mcod.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanasis
 */
public class Packet {
    private int id;
    private Instant arrivalTime;
    private int relativeArrivalTime;
    private String sourceIP;
    private String targetIP;

    public Packet(int id, Instant arrivalTime, int relativeArrivalTime, String sourceIP, String targetIP) {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.relativeArrivalTime = relativeArrivalTime;
        this.sourceIP = sourceIP;
        this.targetIP = targetIP;
    }
    
    public Packet(int id, String arrivalTime, double relativeArrivalTime, String sourceIP, String targetIP) {
        this.id = id;
        try {
            this.arrivalTime = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss.SSSSSSSSS z").parse(arrivalTime).toInstant();
        } catch (ParseException ex) {
            Logger.getLogger(Packet.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.relativeArrivalTime = (int) Math.round(relativeArrivalTime);
        this.sourceIP = sourceIP;
        this.targetIP = targetIP;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Instant getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Instant arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getRelativeArrivalTime() {
        return relativeArrivalTime;
    }

    public void setRelativeArrivalTime(int relativeArrivalTime) {
        this.relativeArrivalTime = relativeArrivalTime;
    }

    public String getSourceIP() {
        return sourceIP;
    }

    public void setSourceIP(String sourceIP) {
        this.sourceIP = sourceIP;
    }

    public String getTargetIP() {
        return targetIP;
    }

    public void setTargetIP(String targetIP) {
        this.targetIP = targetIP;
    }

    public String getPacketIP(){
        return this.getTargetIP();
    }
}
