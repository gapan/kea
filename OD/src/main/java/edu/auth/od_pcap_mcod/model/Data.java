package edu.auth.od_pcap_mcod.model;

import edu.auth.od_pcap_mcod.mcod.DistanceFunctions.EuclideanCoordinate;
import java.util.Random;

import java.time.Instant;

public class Data implements EuclideanCoordinate, Comparable<Data> {
	
	private double[] values;
	public final int hashCode;
	private Packet packet;

	//arrival time 
	private long arrivalTime;
        private Instant actualTime;
        private long outlier_start_time;
        private long outlier_end_time;

    public Data(long arrivalTime, Instant actualTime, Packet packet, double... values) {
        this.values = values;

        int hashCode2 = 1;
        for(double value : values) {
                hashCode2 = 31*hashCode2 + (int)value + (new Random()).nextInt(100000);
        }
        this.hashCode = hashCode2;
        this.arrivalTime = arrivalTime;
        this.actualTime = actualTime;
        this.packet = packet;
    }
        
    public Data(double... values) {
        this.values = values;

        int hashCode2 = 1;
        for(double value : values) {
                hashCode2 = 31*hashCode2 + (int)value + (new Random()).nextInt(100000);
        }
        this.hashCode = hashCode2;
    }

    @Override
    public int dimensions() {
            return values.length;
    }

    @Override
    public double get(int index) {
            return values[index];
    }

    @Override
    public int hashCode() {
            return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
            if(obj instanceof Data) {
                    Data that = (Data) obj;
                    if(this.arrivalTime != that.arrivalTime) return false;
                    if(this.dimensions() != that.dimensions()) {
                            return false;
                    }
                    for(int i = 0; i < this.dimensions(); i++) {
                            if(this.values[i] != that.values[i]) {
                                    return false;
                            }
                    }
                    return true;
            } else {
                    return false;
            }
    }

    @Override
    public int compareTo(Data that) {
            int dimensions = Math.min(this.dimensions(), that.dimensions());
            for(int i = 0; i < dimensions; i++) {
                    double v1 = this.values[i];
                    double v2 = that.values[i];
                    if(v1 > v2) {
                            return +1;
                    }
                    if(v1 < v2) {
                            return -1;
                    }
            }

            if(this.dimensions() > dimensions) {
                    return +1;
            }

            if(that.dimensions() > dimensions) {
                    return -1;
            }

            return 0;
    }

    public double[] getValues() {
        return values;
    }

    public void setValues(double[] values) {
        this.values = values;
    }

    public long getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public long getOutlier_start_time() {
        return outlier_start_time;
    }

    public void setOutlier_start_time(long outlier_start_time) {
        this.outlier_start_time = outlier_start_time;
    }

    public long getOutlier_end_time() {
        return outlier_end_time;
    }

    public void setOutlier_end_time(long outlier_end_time) {
        this.outlier_end_time = outlier_end_time;
    }

    public Instant getActualTime() {
        return actualTime;
    }

    public void setActualTime(Object actualTime) {
        this.actualTime = Instant.parse( actualTime.toString() );
    }
	
    public String getPacketIP(){
        return packet.getPacketIP();
    }

    public Packet getPacket(){
        return packet;
    }

    public void setPacket(Packet packet){
        this.packet = packet;
    }
}