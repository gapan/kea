package edu.auth.od_pcap_mcod.report;

import edu.auth.od_pcap_mcod.model.Data;
import edu.auth.od_pcap_mcod.webapp.DetectionAPIParameters;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanasis
 */
public class ReportManager {
    
    private List<IReporter> reporters;
    private Logger logger;

    public ReportManager(DetectionAPIParameters params) {
        logger = Logger.getLogger(ReportManager.class.getName());
        logger.setLevel(Level.INFO);
        reporters = new ArrayList<>();
        if(!params.getRsyslogServer().equals("")){
            reporters.add(new RsyslogHandler(params.getRsyslogServer(), params.getRsyslogPort()));
        } else if(System.getenv("RSYSLOG_SERVER") != null){
            int rsyslogPort = 514;
            String port = System.getenv("RSYSLOG_PORT");
            if (port != null){
                rsyslogPort = Integer.parseInt(port);
            }
            reporters.add(new RsyslogHandler(System.getenv("RSYSLOG_SERVER"), rsyslogPort));
        }
        reporters.add(new TimescaleDbHandler(params.getTimeDbHost(),
                Integer.parseInt(params.getTimeDbPort()),
                params.getTimeDbUsername(),
                params.getTimeDbPassword(),
                Boolean.parseBoolean(params.getTimeDbSsl()),
                params.getResutlsDB(),
                params.getResultsTable()));
    }
    
    
    public void reportOutlier(Data outlier, String label){
        logger.fine("Reporting Outlier");
        for (IReporter reporter : reporters){
            try {
                reporter.reportOutlier(outlier, label);
            } catch(Exception ex) {
                logger.severe("Couldn't report to Timescale");
            }
        }
    }
    
}
