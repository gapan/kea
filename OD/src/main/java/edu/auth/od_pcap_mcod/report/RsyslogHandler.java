package edu.auth.od_pcap_mcod.report;

import com.cloudbees.syslog.Facility;
import com.cloudbees.syslog.MessageFormat;
import com.cloudbees.syslog.Severity;
import com.cloudbees.syslog.sender.TcpSyslogMessageSender;
import edu.auth.od_pcap_mcod.model.Data;

public class RsyslogHandler implements IReporter {

    private String rsyslogServer;
    private int rsyslogPort;

    public RsyslogHandler(String server, int port) {

        this.rsyslogServer = server;
        this.rsyslogPort = port;
    }

    @Override
    public void reportOutlier(Data outlier, String label) {
        TcpSyslogMessageSender messageSender = new TcpSyslogMessageSender();
        try {
            messageSender.setDefaultMessageHostname("KEA");
            messageSender.setDefaultAppName("KEA-OD");
            messageSender.setDefaultFacility(Facility.USER);
            messageSender.setDefaultSeverity(Severity.CRITICAL);
            messageSender.setSyslogServerHostname(this.rsyslogServer);
            messageSender.setSyslogServerPort(this.rsyslogPort);
            messageSender.setMessageFormat(MessageFormat.RFC_3164); // optional, default is RFC 3164
            messageSender.setSsl(false);
            messageSender.sendMessage(
                    "Asset: " + label
                    + " Target_IP: " + outlier.getPacket().getTargetIP()
                    + " Risk: " + outlier.getValues()[0]
                    + " IncidentDate: " + outlier.getActualTime()
                    + " Incident: Packet loss increased - Possible indication of Dos attack"
                    + " Timeframe: 0 ");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                messageSender.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
