package edu.auth.od_pcap_mcod.report;

import edu.auth.od_pcap_mcod.model.Data;
import edu.auth.od_pcap_mcod.webapp.DetectionAPIParameters;

public interface IReporter {
    public void reportOutlier(Data outlier, String label);
}
