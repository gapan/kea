package edu.auth.od_pcap_mcod.mcod;

import edu.auth.od_pcap_mcod.model.Data;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.List;

/**
 *
 * @author Luan
 */
public class MCOD {

    private HashMap<Long, MCO> dataList_set = new HashMap<>();
    private HashMap<Long, ArrayList<MCO>> micro_clusters = new HashMap<>();
    private ArrayList<MCO> PD = new ArrayList<>();
    // store list ob in increasing time arrival order
    private  ArrayList<MCO> dataList = new ArrayList<>();
    private MTreeClass mtree = new MTreeClass();
    private ArrayList<MCO> outlierList = new ArrayList<>();
    private PriorityQueue<MCO> eventQueue = new PriorityQueue<>(new MCComparator());
    
    private double R;
    private int k;
    private int W;
    private int slide;
    
    public long currentTime = 0; // Naskos change

    public MCOD(double R, int k, int W, int slide) {
        this.R = R;
        this.k = k;
        this.W = W;
        this.slide = slide;
    }

    public ArrayList<Data> detectOutlier(List<Data> data, long currentTime, int W, int slide) {
        this.currentTime = currentTime;//Naskos change
        ArrayList<Data> result = new ArrayList<>();

        if (slide != W) {
            //purge expire object
            for (int i = dataList.size() - 1; i >= 0; i--) {
                MCO d = dataList.get(i);
                if (d.getArrivalTime() <= currentTime - W) {
                    //remove d from data List 
                    dataList.remove(i);

                    //if d is in cluster 
                    if (d.isInCluster) {
                        removeFromCluster(d);
                    }
                    //if d is PD 

                    removeFromPD(d);
                    //process event queue
                    process_event_queue(currentTime);

                }
            }
        } else {
            micro_clusters.clear();
            dataList.clear();
            dataList_set.clear();
            eventQueue.clear();
            mtree = null;
            mtree = new MTreeClass();
            PD.clear();
            outlierList.clear();
        }
        //process new data
        data.stream().forEach((d) -> {
            processNewData(d);
        });

        //add result
        outlierList.stream().forEach((o) -> {
            result.add(o);
        });
        return result;
    }
    
    public double computeAvgNeighborList (){
        double result = 0;
        for(MCO point: PD){
            result += point.exps.size();
        }
        return result/PD.size();
    }

    public int computeNumberOfPointsInCluster() {
        int count = 0;
        for (ArrayList<MCO> points : micro_clusters.values()) {
            count += points.size();
        }
        return count;
    }

//    public void addToHashMap(Integer o1, Integer o2) {
//        ArrayList<Integer> values = checkedPoints.get(o1);
//        if (values != null) {
//            values.add(o2);
//            checkedPoints.put(o1, values);
//        } else {
//            values = new ArrayList<>();
//            values.add(o2);
//            checkedPoints.put(o1, values);
//        }
//    }
//    public boolean checkInHashMap(Integer key, Integer v) {
//        ArrayList<Integer> values = checkedPoints.get(key);
//        return values != null && values.contains(v);
//    }
    private void removeFromCluster(MCO d) {

        //get the cluster
        ArrayList<MCO> cluster = micro_clusters.get(d.center);
        if (cluster != null) {
            cluster.remove(d);
            micro_clusters.put(d.center, cluster);

            //cluster is shrinked 
            if (cluster.size() < k + 1) {
                //remove this cluster from micro cluster list
                micro_clusters.remove(d.center);
                dataList_set.remove(d.center);
                Collections.sort(cluster, new MCComparatorArrivalTime());
                //process the objects in clusters 
                for (int i = 0; i < cluster.size(); i++) {
                    MCO o = cluster.get(i);
                    //reset all objects 
                    resetObject(o);
                    //put into PD 

                    o.numberOfSucceeding = o.numberOfSucceeding + cluster.size() - 1 - i;
                    addToPD(o, true);

                }

            }
        }

    }

    private void removeFromPD(MCO d) {
        //remove from pd
        PD.remove(d);
//        mtree.remove(d);

        //if d is in outlier list 
        if (d.numberOfSucceeding + d.exps.size() < k) {
            if(outlierList.contains(d)){
                d.setOutlier_end_time(currentTime); //Naskos change
            }
            outlierList.remove(d);
        }

        outlierList.stream().forEach((data) -> {
            while (data.exps.size() > 0 && data.exps.get(0) <= d.getArrivalTime() + W) {
                data.exps.remove(0);
                if (data.exps.isEmpty()) {
                    data.ev = 0;
                } else {
                    data.ev = data.exps.get(0);
                }
            }
        });
    }

    private void resetObject(MCO o) {
        o.exps.clear();
        o.Rmc.clear();
        o.isCenter = false;
        o.isInCluster = false;
        o.ev = 0;
        o.center = -1;
        o.numberOfSucceeding = 0;

    }

    public void appendToFile(String filename, String str) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)))) {
            out.println(str);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    private void addToPD(MCO o, boolean fromCluster) {

        PD.stream().forEach((inPD) -> {
            //compute distance
            double distance = mtree.getDistanceFunction().calculate(o, inPD);
            if (distance <= R) {
                //check inPD is succeeding or preceding neighbor
                if (isSameSlide(inPD, o) == -1) {
                    //is preceeding neighbor
                    o.exps.add(inPD.getArrivalTime() + W);
                    if (!fromCluster) {
                        inPD.numberOfSucceeding++;
                    }
                } else if (isSameSlide(inPD, o) == 0) {
                    o.numberOfSucceeding++;
                    if (!fromCluster) {
                        inPD.numberOfSucceeding++;
                    }
                } else {
                    o.numberOfSucceeding++;
                    if (!fromCluster) {
                        inPD.exps.add(o.getArrivalTime() + W);
                    }

                }
                //just keep k-numberofSucceedingNeighbor
                if (!fromCluster) {
                    checkInlier(inPD);
                }

            }
        });

        //find neighbors in clusters (3R/2)
        ArrayList<Long> clusters = findClusterIn3_2Range(o);
        clusters.stream().map((center_id) -> micro_clusters.get(center_id)).forEach((points) -> {
            points.stream().filter((p) -> (isNeighbor(p, o))).forEach((p) -> {
                if (isSameSlide(o, p) <= 0) {
                    //o is preceeding neighbor
                    o.numberOfSucceeding++;
                } else {
                    //p is preceeding neighbor
                    o.exps.add(p.getArrivalTime() + W);
                }
            });
        });

        //keep k-numberofSucceedingNeighbor of o
        checkInlier(o);

        PD.add(o);
//        mtree.add(o);

    }

    public int isSameSlide(MCO o1, MCO o2) {
        if ((o1.getArrivalTime() - 1) / slide == (o2.getArrivalTime() - 1) / slide) {
            return 0;
        } else if ((o1.getArrivalTime() - 1) / slide < (o2.getArrivalTime() - 1) / slide) {
            return -1;
        } else {
            return 1;
        }
    }

    public long findNearestCenter(MCO d) {

        double min_distance = Double.MAX_VALUE;
        long min_center_id = -1;
        for (Long center_id : micro_clusters.keySet()) {
            //get the center object
            MCO center = dataList_set.get(center_id);
            //compute the distance
            double distance = mtree.getDistanceFunction().calculate(center, d);

            if (distance < min_distance) {
                min_distance = distance;
                min_center_id = center_id;
            }
        }
        return min_center_id;

    }

    public ArrayList<Long> findClusterIn3_2Range(MCO d) {
        ArrayList<Long> result = new ArrayList<>();
        micro_clusters.keySet().stream().forEach((center_id) -> {
            //get the center object
            MCO center = dataList_set.get(center_id);
            //compute the distance
            double distance = mtree.getDistanceFunction().calculate(center, d);
            if (distance <= R * 3.0 / 2) {
                result.add(center_id);
            }
        });
        return result;
    }

    private void processNewData(Data data) {

        MCO d = new MCO(data);

        //add to datalist
        dataList.add(d);

        long nearest_center_id = findNearestCenter(d);
        double min_distance = Double.MAX_VALUE;
        if (nearest_center_id > -1) { //found neareast cluster
            min_distance = mtree.getDistanceFunction().
                    calculate(dataList_set.get(nearest_center_id), d);
        }
        //assign to cluster if min distance <= R/2
        if (min_distance <= R / 2) {
            addToCluster(nearest_center_id, d);
        } else {
            //find all neighbors for d in PD that can  form a cluster
            ArrayList<MCO> neighborsInR2Distance = findNeighborR2InPD(d);
            if (neighborsInR2Distance.size() >= k * 1.1) {
                //form new cluster
                formNewCluster(d, neighborsInR2Distance);

            } else {
                //cannot form a new cluster
                addToPD(d, false);
            }
        }
        if (d.isCenter) {
            dataList_set.put(d.getArrivalTime(), d);
        }

    }

    private void addToCluster(long nearest_center_id, MCO d) {

        //update for points in cluster
        d.isCenter = false;
        d.isInCluster = true;
        d.center = nearest_center_id;
        ArrayList<MCO> cluster = micro_clusters.get(nearest_center_id);
        cluster.add(d);
        micro_clusters.put(nearest_center_id, cluster);

        //update for points in PD that has Rmc list contains center
        PD.stream().filter((inPD) -> (inPD.Rmc.contains(nearest_center_id))).forEach((inPD) -> {
            //check if inPD is neighbor of d
            double distance = mtree.getDistanceFunction().
                    calculate(d, inPD);
            if (distance <= R) {
                if (isSameSlide(d, inPD) == -1) {
                    inPD.exps.add(d.getArrivalTime() + W);

                } else if (isSameSlide(d, inPD) >= 0) {
                    inPD.numberOfSucceeding++;
                }
                //mark inPD has checked with d
//                    addToHashMap(inPD.arrivalTime,d.arrivalTime);
                //check if inPD become inlier
                checkInlier(inPD);
            }
        });

    }

    public ArrayList<MCO> findNeighborR2InPD(MCO d) {
        ArrayList<MCO> results = new ArrayList<>();
        PD.stream().filter((o) -> (mtree.getDistanceFunction().calculate(o, d) <= R * 1.0 / 2)).forEach((o) -> {
            results.add(o);
        });
        return results;
    }

    public boolean isOutlier(MCO d) {
        return d.numberOfSucceeding + d.exps.size() < k;
    }

    private void formNewCluster(MCO d, ArrayList<MCO> neighborsInR2Distance) {

        d.isCenter = true;
        d.isInCluster = true;
        d.center = d.getArrivalTime();
        neighborsInR2Distance.stream().map((data) -> {
            PD.remove(data);
            return data;
        }).map((data) -> {
            if (isOutlier(data)) {
                if(outlierList.contains(data)){
                    data.setOutlier_end_time(currentTime); //Naskos change
                }
                outlierList.remove(data);
            }
            return data;
        }).map((data) -> {
            if (!isOutlier(data)) {
                eventQueue.remove(data);
            }
            return data;
        }).map((data) -> {
            resetObject(data);
            return data;
        }).map((data) -> {
            data.isInCluster = true;
            return data;
        }).map((data) -> {
            data.center = d.getArrivalTime();
            return data;
        }).forEach((data) -> {
            data.isCenter = false;
        });

        //add center to neighbor list
        Collections.sort(neighborsInR2Distance, new MCComparatorArrivalTime());
        neighborsInR2Distance.add(d);
        micro_clusters.put(d.getArrivalTime(), neighborsInR2Distance);

        //update Rmc list
        ArrayList<MCO> list_rmc = findNeighborInR3_2InPD(d);
        list_rmc.stream().map((o) -> {
            if (isNeighbor(o, d)) {
                if (isSameSlide(o, d) <= 0) {
                    o.numberOfSucceeding++;
                } else {
                    o.exps.add(d.getArrivalTime() + W);
                }
//                addToHashMap(o.arrivalTime,d.arrivalTime);
                checkInlier(o);

            }
            return o;
        }).forEach((o) -> {
            o.Rmc.add(d.getArrivalTime());
        });

    }

    private ArrayList<MCO> findNeighborInRInPD(MCO d) {

        ArrayList<MCO> result = new ArrayList<>();

        PD.stream().filter((o) -> (mtree.getDistanceFunction().calculate(o, d) <= R)).forEach((o) -> {
            result.add(o);
        });
        return result;
    }

    private ArrayList<MCO> findNeighborInR3_2InPD(MCO d) {

        ArrayList<MCO> result = new ArrayList<>();

        PD.stream().forEach((p) -> {
            double distance = mtree.getDistanceFunction().calculate(p, d);
            if (distance <= R * 3.0 / 2) {
                result.add(p);
            }
        });
        return result;
    }

    private void checkInlier(MCO inPD) {
        Collections.sort(inPD.exps);

        while (inPD.exps.size() > k - inPD.numberOfSucceeding && inPD.exps.size() > 0) {
            inPD.exps.remove(0);
        }
        if (inPD.exps.size() > 0) {
            inPD.ev = inPD.exps.get(0);
        } else {
            inPD.ev = 0;
        }

        if (inPD.exps.size() + inPD.numberOfSucceeding >= k) {
            if (inPD.numberOfSucceeding >= k) {

                eventQueue.remove(inPD);
                
                if(outlierList.contains(inPD)){
                    inPD.setOutlier_end_time(currentTime); //Naskos change
                }
                outlierList.remove(inPD);
            } else {
                if(outlierList.contains(inPD)){
                    inPD.setOutlier_end_time(currentTime); //Naskos change
                }
                outlierList.remove(inPD);
                if (!eventQueue.contains(inPD)) {
                    eventQueue.add(inPD);
                }
            }

        } else {
            eventQueue.remove(inPD);
            if (!outlierList.contains(inPD)) {
                outlierList.add(inPD);
                if(inPD.getOutlier_start_time() == 0){  
                    inPD.setOutlier_start_time(currentTime); //Naskos change
                }
            }
        }
    }

    private boolean isNeighbor(MCO p, MCO o) {
        double d = mtree.getDistanceFunction().calculate(p, o);
        return d <= R;
    }

    private void process_event_queue(long currentTime) {
        MCO x = eventQueue.peek();

        while (x != null && x.ev <= currentTime) {

            x = eventQueue.poll();
            while (x.exps.get(0) <= currentTime) {
                x.exps.remove(0);
                if (x.exps.isEmpty()) {
                    x.ev = 0;
                    break;
                } else {
                    x.ev = x.exps.get(0);
                }
            }
            if (x.exps.size() + x.numberOfSucceeding < k) {

                outlierList.add(x);
                if(x.getOutlier_start_time() == 0){  
                    x.setOutlier_start_time(currentTime); //Naskos change
                }

            } else if (x.numberOfSucceeding < k
                    && x.exps.size() + x.numberOfSucceeding >= k) {
                eventQueue.add(x);
            }

            x = eventQueue.peek();

        }
    }

    static class MCComparator implements Comparator<MCO> {

        @Override
        public int compare(MCO o1, MCO o2) {
            if (o1.ev < o2.ev) {
                return -1;
            } else if (o1.ev == o2.ev) {
                return 0;
            } else {
                return 1;
            }

        }

    }

    static class MCComparatorArrivalTime implements Comparator<MCO> {

        @Override
        public int compare(MCO o1, MCO o2) {
            if (o1.getArrivalTime() < o2.getArrivalTime()) {
                return -1;
            } else if (o1.getArrivalTime() == o2.getArrivalTime()) {
                return 0;
            } else {
                return 1;
            }

        }

    }

    static class MCO extends Data {

        public long center;
        public ArrayList<Long> exps;
        public ArrayList<Long> Rmc;

        public long ev;
        public boolean isInCluster;
        public boolean isCenter;

        public int numberOfSucceeding;

        public MCO(Data d) {
            super();
            this.setArrivalTime(d.getArrivalTime());
            this.setActualTime(d.getActualTime());
            this.setValues(d.getValues());

            exps = new ArrayList<>();
            Rmc = new ArrayList<>();
            isCenter = false;
            isInCluster = false;
        }

    }

    public int getDataListSize() {
        return dataList.size();
    }
    
    
    
}
