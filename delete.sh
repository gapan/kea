#!/bin/bash
# Stop containers and removes containers, networks, volumes, and images 
docker-compose -f docker-compose-jfrog.yml down
docker volume ls | \
	grep kea_ | \
	tr -s ' ' | \
	cut -d' ' -f2 | \
	xargs docker volume rm

# Wait a bit 
sleep 5s

# Check that containers were removed succesfully
res=`docker ps -a | grep "/curex-local/kea_" | wc -l`
if [ $res -ne 0 ]; then
	echo "ERROR: Some KEA containers have not been removed."
	docker ps -a
	exit 1
fi

